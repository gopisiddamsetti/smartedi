package com.smartedi.commons;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestService {
	
	
	public static void main(String[] args) {
		

		
		//--------------------------------------------success Arcesb---------------
				Map map = new HashMap<>();
				map.put("Filename", "EDIdata850-Success_000003438.response");
				map.put("Id", "4033");
				map.put("Workspace", "In850andOut810");
				map.put("PortId", "Amazon_FTP_X124010850_Rest_Inbound");
				map.put("Status", "Received");
				map.put("ConnectorId", "Amazon_FTP_X124010850_Rest_Inbound");
				map.put("MessageId", "Amazon_FTPServer-20210210-064906779-x1ew");
				map.put("Timestamp", "2021-02-10T06:49:08.0000+00:00");
				map.put("ETag", "");
				map.put("Direction", "Receive");
				map.put("FileSize", "12");
				map.put("FilePath", "/workspaces/In850andOut810/Rest_amq/Receive/EDIdata850-Success_0000034381.response");

				Map map1 = new HashMap<>();
				map1.put("Filename", "EDIdata850-Success_000003438.xml");
				map1.put("Id", "4034");
				map1.put("Workspace", "In850andOut810");
				map1.put("PortId", "Amazon_FTP_X124010850_Rest_Inbound");
				map1.put("Status", "Sent");
				map1.put("ConnectorId", "Amazon_FTP_X124010850_Rest_Inbound");
				map1.put("MessageId", "Amazon_FTPServer-20210210-064906779-x1ew");
				map1.put("Timestamp", "2021-02-10T06:49:08.0000+00:00");
				map1.put("ETag", "");
				map1.put("Direction", "Send");
				map1.put("FileSize", "14554");
				map1.put("FilePath", "/workspaces/In850andOut810/Rest_amq/Sent/EDIdata850-Success_0000034381.xml");

				Map map2 = new HashMap<>();
				map2.put("Filename", "EDIdata850-Success_000003438.xml");
				map2.put("Id", "4029");
				map2.put("Workspace", "In850andOut810");
				map2.put("PortId", "Amazon_FTP_X124010850_X12_Inbound");
				map2.put("Status", "Received");
				map2.put("ConnectorId", "Amazon_FTP_X124010850_X12_Inbound");
				map2.put("MessageId", "Amazon_FTPServer-20210210-064906779-x1ew");
				map2.put("Timestamp", "2021-02-10T06:49:07.0000+00:00");
				map2.put("ETag", "");
				map2.put("Direction", "Receive");
				map2.put("FileSize", "14213");
				map2.put("FilePath", "/workspaces/In850andOut810/Inbound_X124010850/Receive/EDIdata850-Success_000003438.xml");

				Map map3 = new HashMap<>();
				map3.put("Filename", "EDIdata850-Success.edi");
				map3.put("Id", "4030");
				map3.put("Workspace", "In850andOut810");
				map3.put("PortId", "Amazon_FTP_X124010850_X12_Inbound");
				map3.put("Status", "Sent");
				map3.put("ConnectorId", "Amazon_FTP_X124010850_X12_Inbound");
				map3.put("MessageId", "Amazon_FTPServer-20210210-064906779-x1ew");
				map3.put("Timestamp", "2021-02-10T06:49:07.0000+00:00");
				map3.put("ETag", "");
				map3.put("Direction", "Send");
				map3.put("FileSize", "1133");
				map3.put("FilePath", "/workspaces/In850andOut810/Inbound_X124010850/Sent/EDIdata850-Success2.edi");

				Map map4 = new HashMap<>();
				map4.put("Filename", "EDIdata850-Success_000003438.xml");
				map4.put("Id", "4031");
				map4.put("Workspace", "In850andOut810");
				map4.put("PortId", "Amazon_FTP_X124010850_Wrapper_Inbound");
				map4.put("Status", "Received");
				map4.put("ConnectorId", "Amazon_FTP_X124010850_Wrapper_Inbound");
				map4.put("MessageId", "Amazon_FTPServer-20210210-064906779-x1ew");
				map4.put("Timestamp", "2021-02-10T06:49:07.0000+00:00");
				map4.put("ETag", "");
				map4.put("Direction", "Receive");
				map4.put("FileSize", "14554");
				map4.put("FilePath",
						"/workspaces/In850andOut810/Amazon_X124010850_Wrapper/Receive/EDIdata850-Success_000003438.xml");

				Map map5 = new HashMap<>();
				map5.put("Filename", "EDIdata850-Success_000003438.xml");
				map5.put("Id", "4032");
				map5.put("Workspace", "In850andOut810");
				map5.put("PortId", "Amazon_FTP_X124010850_Wrapper_Inbound");
				map5.put("Status", "Sent");
				map5.put("ConnectorId", "Amazon_FTP_X124010850_Wrapper_Inbound");
				map5.put("MessageId", "Amazon_FTPServer-20210210-064906779-x1ew");
				map5.put("Timestamp", "2021-02-10T06:49:07.0000+00:00");
				map5.put("ETag", "");
				map5.put("Direction", "Send");
				map5.put("FileSize", "14213");
				map5.put("FilePath",
						"/workspaces/In850andOut810/Amazon_X124010850_Wrapper/Sent/EDIdata850-Success_0000034381.xml");

				Map map6 = new HashMap<>();
				map6.put("Filename", "EDIdata850-Success.edi");
				map6.put("Id", "4028");
				map6.put("Workspace", "In850andOut810");
				map6.put("PortId", "Amazon_FTP_X124010850_FTPServer_Inbound");
				map6.put("Status", "Received");
				map6.put("ConnectorId", "Amazon_FTP_X124010850_FTPServer_Inbound");
				map6.put("MessageId", "Amazon_FTPServer-20210210-064906779-x1ew");
				map6.put("Timestamp", "2021-02-10T06:49:06.0000+00:00");
				map6.put("ETag", "");
				map6.put("Direction", "Receive");
				map6.put("FileSize", "1133");
				map6.put("FilePath", "/ftpserver/Amazon_FTPServer/Receive/EDIdata850-Success.edi");
				
				
				//--------------------------------------------success Arcesb---------------
				
				
				
				
				
				
				
				//--------------------------------------------OutBound success---------------
				Map map11 = new HashMap<>();
				map11.put("Filename","In850andOut810_X124010810_Webhook_Outbound_2021-02-09T14-38-28_000000171.x12");
				map11.put("Id","4020");
				map11.put("Workspace","In850andOut810");
				map11.put("PortId","Amazon_FTP_X124010810_FTP_Outbound");
				map11.put("Status","Sent");
				map11.put("ConnectorId","Amazon_FTP_X124010810_FTP_Outbound");
				map11.put("MessageId","In850andOut810_X124010810_Webhook_Outbound-20210209-143828279-8xGD");
				map11.put("Timestamp","2021-02-09T14:38:30.0000+00:00");
				map11.put("ETag","");
				map11.put("Direction","Send");
				map11.put("FileSize","569");
				map11.put("FilePath","/workspaces/In850andOut810/Amazon_FTP/Sent/In850andOut810_X124010810_Webhook_Outbound_2021-02-09T14-38-28_000000171.x12");
				
				Map map22 = new HashMap<>();       
				map22.put("Filename","In850andOut810_X124010810_Webhook_Outbound_2021-02-09T14-38-28_000000171.x12");
				map22.put("Id","4017");
				map22.put("Workspace","In850andOut810");
				map22.put("PortId","Amazon_FTP_X124010810_X12_Outbound");
				map22.put("Status","Received");
				map22.put("ConnectorId","Amazon_FTP_X124010810_X12_Outbound");
				map22.put("MessageId","In850andOut810_X124010810_Webhook_Outbound-20210209-143828279-8xGD");
				map22.put("Timestamp","2021-02-09T14:38:29.0000+00:00");
				map22.put("ETag","");
				map22.put("Direction","Receive");
				map22.put("FileSize","569");
				map22.put("FilePath","/workspaces/In850andOut810/X12_OutBound/Receive/In850andOut810_X124010810_Webhook_Outbound_2021-02-09T14-38-28_000000171.x12");
				
				Map map33 = new HashMap<>();
				map33.put("Filename","In850andOut810_X124010810_Webhook_Outbound_2021-02-09T14-38-28.xml");
				map33.put("Id","4018");
				map33.put("Workspace","In850andOut810");
				map33.put("PortId","Amazon_FTP_X124010810_X12_Outbound");
				map33.put("Status","Sent");
				map33.put("ConnectorId","Amazon_FTP_X124010810_X12_Outbound");
				map33.put("MessageId","In850andOut810_X124010810_Webhook_Outbound-20210209-143828279-8xGD");
				map33.put("Timestamp","2021-02-09T14:38:29.0000+00:00");
				map33.put("ETag","");
				map33.put("Direction","Send");
				map33.put("FileSize","15061");
				map33.put("FilePath","/workspaces/In850andOut810/X12_OutBound/Sent/In850andOut810_X124010810_Webhook_Outbound_2021-02-09T14-38-28.xml");
				
				Map map44 = new HashMap<>();      
				map44.put("Filename","In850andOut810_X124010810_Webhook_Outbound_2021-02-09T14-38-28.xml");
				map44.put("Id","4014");
				map44.put("Workspace","In850andOut810");
				map44.put("PortId","Amazon_FTP_X124010810_Webhook_Outbound");
				map44.put("Status","Received");
				map44.put("ConnectorId","Amazon_FTP_X124010810_Webhook_Outbound");
				map44.put("MessageId","In850andOut810_X124010810_Webhook_Outbound-20210209-143828279-8xGD");
				map44.put("Timestamp","2021-02-09T14:38:28.0000+00:00");
				map44.put("ETag","");
				map44.put("Direction","Receive");
				map44.put("FileSize","3623");
				map44.put("FilePath","/workspaces/In850andOut810/In850andOut810_X124010810_Webhook_Outbound/Receive/In850andOut810_X124010810_Webhook_Outbound_2021-02-09T14-38-28.xml");
				
				Map map55 = new HashMap<>();      
				map55.put("Filename","In850andOut810_X124010810_Webhook_Outbound_2021-02-09T14-38-28.xml");
				map55.put("Id","4015");
				map55.put("Workspace","In850andOut810");
				map55.put("PortId","Amazon_FTP_X124010810_XmlMap_Outbound");
				map55.put("Status","Received");
				map55.put("ConnectorId","Amazon_FTP_X124010810_XmlMap_Outbound");
				map55.put("MessageId","In850andOut810_X124010810_Webhook_Outbound-20210209-143828279-8xGD");
				map55.put("Timestamp","2021-02-09T14:38:28.0000+00:00");
				map55.put("ETag","");
				map55.put("Direction","Receive");
				map55.put("FileSize","15061");
				map55.put("FilePath","/workspaces/In850andOut810/XMLMap_Outbound/Receive/In850andOut810_X124010810_Webhook_Outbound_2021-02-09T14-38-28.xml");
				
				Map map66 = new HashMap<>();     
				map66.put("Filename","In850andOut810_X124010810_Webhook_Outbound_2021-02-09T14-38-28.xml");
				map66.put("Id","4016");
				map66.put("Workspace","In850andOut810");
				map66.put("PortId","Amazon_FTP_X124010810_XmlMap_Outbound");
				map66.put("Status","Sent");
				map66.put("ConnectorId","Amazon_FTP_X124010810_XmlMap_Outbound");
				map66.put("MessageId","In850andOut810_X124010810_Webhook_Outbound-20210209-143828279-8xGD");
				map66.put("Timestamp","2021-02-09T14:38:28.0000+00:00");
				map66.put("ETag","");
				map66.put("Direction","Send");
				map66.put("FileSize","3623");
				map66.put("FilePath","/workspaces/In850andOut810/XMLMap_Outbound/Sent/In850andOut810_X124010810_Webhook_Outbound_2021-02-09T14-38-28.xml");
				       

				//--------------------------------------------OutBound success---------------
				
				
				//--------------------------------------------InBound Arc fail---------------
				Map map21 = new HashMap<>();     
				map21.put("Filename","EDIdata850-Failed.edi");
				map21.put("Id","4035");
				map21.put("Workspace","In850andOut810");
				map21.put("PortId","Amazon_FTP_X124010850_FTPServer_Inbound");
				map21.put("Status","Received");
				map21.put("ConnectorId","Amazon_FTP_X124010850_FTPServer_Inbound");
				map21.put("MessageId","Amazon_FTPServer-20210210-065648169-zmMx");
				map21.put("Timestamp","2021-02-10T06:56:48.0000+00:00");
				map21.put("ETag","");
				map21.put("Direction","Receive");
				map21.put("FileSize", "1132");
				map21.put("FilePath","/ftpserver/Amazon_FTPServer/Receive/EDIdata850-Failed.edi");
				       
				Map map222 = new HashMap<>();     
				map222.put("Filename","EDIdata850-Failed.edi");
				map222.put("Id","4036");
				map222.put("Workspace","In850andOut810");
				map222.put("PortId","Amazon_FTP_X124010850_X12_Inbound");
				map222.put("Status","Send Error");
				map222.put("ConnectorId","Amazon_FTP_X124010850_X12_Inbound");
				map222.put("MessageId","Amazon_FTPServer-20210210-065648169-zmMx");
				map222.put("Timestamp","2021-02-10T06:56:48.0000+00:00");
				map222.put("ETag","");
				map222.put("Direction","Send");
				map222.put("FileSize","1132");
				map222.put("FilePath","/workspaces/In850andOut810/Inbound_X124010850/Send/Amazon_FTPServer-20210210-065648169-zmMx.eml");
				//--------------------------------------------InBound Arc fail---------------

				
				
				
				
				

				List<Map<String, String>> list = new ArrayList<>();
/*				list.add(MapUtil.transformLowerCase(map));
				list.add(MapUtil.transformLowerCase(map1));
				list.add(MapUtil.transformLowerCase(map4));
				list.add(MapUtil.transformLowerCase(map3));
				list.add(MapUtil.transformLowerCase(map6));
				list.add(MapUtil.transformLowerCase(map5));
				list.add(MapUtil.transformLowerCase(map2));*/
				
			/*	list.add(MapUtil.transformLowerCase(map11));
				list.add(MapUtil.transformLowerCase(map22));
				list.add(MapUtil.transformLowerCase(map33));
				list.add(MapUtil.transformLowerCase(map44));
				list.add(MapUtil.transformLowerCase(map55));
				list.add(MapUtil.transformLowerCase(map66));*/
				
				
				list.add(MapUtil.transformLowerCase(map21));
				list.add(MapUtil.transformLowerCase(map222));
			
				
				
		
				
				
				for (Map<String, String> map8 : CustomLogs.getFormatLog( list, "errorMsg edi", "hsfhd12351jhgh", "216548464")) {
				System.out.println(map8.get("logMsg"));
			    }
				
	}
	
	
	
	
	
	
	
	
	
	

	

}
