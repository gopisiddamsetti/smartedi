package com.smartedi.commons;


	import java.time.format.DateTimeFormatter;
	import java.time.LocalDateTime;  
	public class DateUtil { 
	  public static void main(String[] args) {  

	   System.out.println(DateUtil.getCurrentDate());
	  }  
	  
	  
	  public static String getCurrentDate() {  
		 // "MMM DD, yyyy @ HH:mm:ss"
		 // 2021-01-27T08:22:25.0000+00:00
		   DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-mm-dd:HH:mm:ss");
		   LocalDateTime now = LocalDateTime.now();
		 
		   
		   String date=dtf.format(now);
		
		   
		return date;
		  }  
	  
	}  

