package com.smartedi.commons;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class MapUtil {
	public static void main(String[] args) {
		 
 /*       Map<String, Object> m3 = new HashMap<String, Object>();
        m3.put("a", "abc");
        m3.put("b", "123");
        m3.put("C", "123");
        m3.put("dD", "123");
 
        Map<String, Object> m4 = new HashMap<String, Object>();
        m4.put("b", "123");
        m4.put("a", "abc");
        m4.put("cDs", "123");
        m4.put("d", "123");
        
 System.out.println ("Convert all m4 data keys to uppercase ===" + transformUpperCase (m4));
 System.out.println ("Convert all keys of m3 data to lower case ===" + transformLowerCase (m4));*/
 
    }
 
 // Convert all map values ​​to uppercase
    public static Map<String, Object> transformUpperCase(Map<String, Object> orgMap) {
        Map<String, Object> resultMap = new HashMap<>();
        if (orgMap == null || orgMap.isEmpty()) {
            return resultMap;
        }
        Set<String> keySet = orgMap.keySet();
        for (String key : keySet) {
            String newKey = key.toUpperCase();
//            newKey = newKey.replace("_", "");
            resultMap.put(newKey, orgMap.get(key));
        }
        return resultMap;
    }
 
 // Convert all map values ​​to lower case
    public static Map<String, String> transformLowerCase(Map<String, String> orgMap) {
        Map<String, String> resultMap = new HashMap<>();
        if (orgMap == null || orgMap.isEmpty()) {
            return resultMap;
        }
        Set<String> keySet = orgMap.keySet();
        for (String key : keySet) {
            String newKey = key.toLowerCase();
//            newKey = newKey.replace("_", "");
            resultMap.put(newKey, orgMap.get(key));
        }
        return resultMap;
    }

}
