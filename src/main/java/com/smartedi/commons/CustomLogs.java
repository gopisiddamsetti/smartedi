package com.smartedi.commons;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CustomLogs {
	public static List<Map<String, String>> getFormatLog(List<Map<String, String>> list,String errorMsg,String transactionId,String businessTrxId) {

		
		
		
		
		

		Collections.sort(list, new Comparator<Map<String, String>>() {
			public int compare(final Map<String, String> o1, final Map<String, String> o2) {
				return o1.get("portid").compareTo(o2.get("portid"));
			}
		});

		/*
		 * Amazon_FTP_X124010850_FTP_Inbound Amazon_FTP_X124010850_Rest_Inbound
		 * Amazon_FTP_X124010850_Rest_Inbound Amazon_FTP_X124010850_Wrapper_Inbound
		 * Amazon_FTP_X124010850_Wrapper_Inbound Amazon_FTP_X124010850_X12_Inbound
		 * Amazon_FTP_X124010850_X12_Inbound
		 */

		int ftpServerError = 0;
		int sftpServerError = 0;
		int as2Error = 0;
		int restError = 0;
		int wrapperError = 0;
		int x12Error = 0;
		
		int ftpServerSuccess = 0;
		int sftpServerSuccess = 0;
		int as2Success = 0;
		int restSuccess = 0;
		int wrapperSuccess = 0;
		int x12Success = 0;
		
		
		
		
		
		
		int webhookOutboundError= 0;
		int xmlMapOutboundError= 0;
		int x12OutboundError= 0;
		int fTPOutboundError= 0;
		int sfTPOutboundError= 0;
		int as2OutboundError= 0;
		
		int 	webhookOutboundSuccess= 0;
		int xmlMapOutboundSuccess= 0;
		int x12OutboundSuccess= 0;
		int 	fTPOutboundSuccess= 0;
		int 	sfTPOutboundSuccess= 0;
		int 	as2OutboundSuccess= 0;
		
		String patner="";
		patner=list.get(0).get("portid").substring(0,list.get(0).get("portid").indexOf("_"));

		for (Map<String, String> map8 : list) {
			

			if (map8.get("status").toString().contains("Error")) {
				if (map8.get("status").toString().contains("Send Error")
						|| map8.get("status").toString().contains("Receive Error")) {

					if (map8.get("portid").endsWith("_SFTPServer_Inbound")) {
						sftpServerError++;

					}
					
					if (map8.get("portid").endsWith("_AS2")) {
						as2Error++;

					}
					if (map8.get("portid").endsWith("_FTPServer_Inbound")) {
						ftpServerError++;

					}
					
					
					
					
					
					if (map8.get("portid").endsWith("_Rest_Inbound")) {
						restError++;

					}

					if (map8.get("portid").endsWith("_Wrapper_Inbound")) {

						wrapperError++;

					}

					if (map8.get("portid").endsWith("_X12_Inbound")) {
						x12Error++;

					}
					
				

				
					
					//---------------outbound-----------------------
					if (map8.get("portid").endsWith("_SFTP_Outbound")) {
						sfTPOutboundError++;

					}
					
					if (map8.get("portid").endsWith("_FTP_Outbound")) {
						fTPOutboundError++;

					}
					if (map8.get("portid").endsWith("_AS2")) {
						as2OutboundError++;

					}
					
					
					
					if (map8.get("portid").endsWith("_X12_Outbound")) {
						x12OutboundError++;

					}

					if (map8.get("portid").endsWith("_XmlMap_Outbound")) {

						xmlMapOutboundError++;

					}

					if (map8.get("portid").endsWith("_Webhook_Outbound")) {
						webhookOutboundError++;

					}
					
					
					//---------------outbound-----------------------
					
					

				}

			} else {

				if (map8.get("portid").endsWith("_FTPServer_Inbound")) {
					
					ftpServerSuccess++;

				}
	if (map8.get("portid").endsWith("_SFTPServer_Inbound")) {
					
					sftpServerSuccess++;

				}
	if (map8.get("portid").endsWith("_AS2")) {
		
		as2Success++;

	}
				
				
				
				
				
				if (map8.get("portid").endsWith("_Rest_Inbound")) {
					restSuccess++;

				}

				if (map8.get("portid").endsWith("_Wrapper_Inbound")) {

					wrapperSuccess++;

				}

				if (map8.get("portid").endsWith("_X12_Inbound")) {
					x12Success++;

				}
				
				
				
				//---------------outbound-----------------------
				if (map8.get("portid").endsWith("_FTP_Outbound")) {
					fTPOutboundSuccess++;
System.out.println("_FTP_Outbound");
				}
				
				if (map8.get("portid").endsWith("_SFTP_Outbound")) {
					sfTPOutboundSuccess++;
				}
				
				if (map8.get("portid").endsWith("_AS2")) {
					as2OutboundSuccess++;
				}
				
				
				
				if (map8.get("portid").endsWith("_X12_Outbound")) {
					x12OutboundSuccess++;

				}

				if (map8.get("portid").endsWith("_XmlMap_Outbound")) {

					xmlMapOutboundSuccess++;

				}

				if (map8.get("portid").endsWith("_Webhook_Outbound")) {
					webhookOutboundSuccess++;

				}
				
				
				//---------------outbound-----------------------
				
				
				

			}

		}

/*		System.out.println("---" + ftpError);
		System.out.println("---" + restError);
		System.out.println("---" + wrapperError);
		System.out.println("---" + x12Error);
		System.out.println("---" + ftpSuccess);
		System.out.println("---" + restSuccess);
		System.out.println("---" + wrapperSuccess);
		System.out.println("---" + x12Success);*/

		
		List<Map<String, String>> result = new ArrayList<>();
		
		
	
		
		
		
		//success check
		
		if (ftpServerSuccess > 0 && ftpServerError==0 ) {
			result.add(getFormatMap( transactionId, businessTrxId, "FTPServer_Inbound", "",patner));
		}
		
		if (sftpServerSuccess > 0 && sftpServerError==0 ) {
			result.add(getFormatMap( transactionId, businessTrxId, "SFTPServer_Inbound", "",patner));
		}
		if (as2Success > 0 && as2Error==0 ) {
			result.add(getFormatMap( transactionId, businessTrxId, "AS2_Inbound", "",patner));
		}
		
		
		if (restSuccess > 0 && restError==0 ) {
			result.add(getFormatMap( transactionId, businessTrxId, "Rest_Inbound", "",patner));
		
		}
		if (wrapperSuccess > 0 && wrapperError==0 ) {
			result.add(getFormatMap( transactionId, businessTrxId, "Wrapper_Inbound", "",patner));
			
		}
		if (x12Success > 0 && x12Error==0 ) {
			result.add(getFormatMap( transactionId, businessTrxId, "X12_Inbound", "",patner));
			
		}
		
		
		//---------------outbound-----------------------
			
		
		if (webhookOutboundSuccess > 0 && webhookOutboundError==0 ) {
			result.add(getFormatMap( transactionId, businessTrxId, "Webhook_Outbound", "",patner));
           
		}
		if (xmlMapOutboundSuccess > 0 && xmlMapOutboundError==0 ) {
			result.add(getFormatMap( transactionId, businessTrxId, "XmlMap_Outbound", "",patner));
			
		}
		if (x12OutboundSuccess > 0 && x12OutboundError==0 ) {
			result.add(getFormatMap( transactionId, businessTrxId, "X12_Outbound", "",patner));
			
		}
		if (fTPOutboundSuccess > 0 && fTPOutboundError==0 ) {
			result.add(getFormatMap( transactionId, businessTrxId, "FTP_Outbound", "",patner));
			
		}
		
		if (sfTPOutboundSuccess > 0 && sfTPOutboundError==0 ) {
			result.add(getFormatMap( transactionId, businessTrxId, "SFTP_Outbound", "",patner));
			
		}
		if (as2OutboundSuccess > 0 && as2OutboundError==0 ) {
			result.add(getFormatMap( transactionId, businessTrxId, "AS2_Outbound", "",patner));
			
		}
		
		
		//---------------outbound-----------------------
		
		
		//Error check
		
		if (ftpServerError > 0) {
			result.add(getFormatMap( transactionId, businessTrxId, "FTPServer_Inbound_Err", errorMsg,patner));
           
		}
		if (sftpServerError > 0) {
			result.add(getFormatMap( transactionId, businessTrxId, "SFTPServer_Inbound_Err", errorMsg,patner));
           
		}
		if (as2Error > 0) {
			result.add(getFormatMap( transactionId, businessTrxId, "AS2_Inbound_Err", errorMsg,patner));
           
		}
		
		
		
		
		if (restError > 0) {
			result.add(getFormatMap( transactionId, businessTrxId, "Rest_Inbound_Err", errorMsg,patner));
			
		}
		if (wrapperError > 0) {
			result.add(getFormatMap( transactionId, businessTrxId, "Wrapper_Inbound_Err", errorMsg,patner));
			
		}
		if (x12Error > 0 ) {
			result.add(getFormatMap( transactionId, businessTrxId, "X12_Inbound_Err", errorMsg,patner));
			
		}
		
		//---------------outbound-----------------------
		
	
		

		
		if (webhookOutboundError > 0) {
			result.add(getFormatMap( transactionId, businessTrxId, "Webhook_Outbound_Err", errorMsg,patner));
           
		}
		if (xmlMapOutboundError > 0) {
			result.add(getFormatMap( transactionId, businessTrxId, "XmlMap_Outbound_Err", errorMsg,patner));
			
		}
		if (x12OutboundError > 0) {
			result.add(getFormatMap( transactionId, businessTrxId, "X12_Outbound_Err", errorMsg,patner));
			
		}
		if (fTPOutboundError > 0 ) {
			result.add(getFormatMap( transactionId, businessTrxId, "FTP_Outbound_Err", errorMsg,patner));
			
		}
		if (sfTPOutboundError > 0 ) {
			result.add(getFormatMap( transactionId, businessTrxId, "SFTP_Outbound_Err", errorMsg,patner));
			
		}
		if (as2OutboundError > 0 ) {
			result.add(getFormatMap( transactionId, businessTrxId, "AS2_Outbound_Err", errorMsg,patner));
			
		}
		//---------------outbound-----------------------
		
		
		
		
		
		
		
		
		
		return result;
		
		
		
		

	}
	
	public static Map<String,String> getFormatMap(String transactionId,String businessTrxId,String connector,String errorMsg,String partner) {
		System.out.println("1-----------------"+transactionId);
		System.out.println("2-----------------"+businessTrxId);
		System.out.println("3-----------------"+connector);
		System.out.println("4-----------------"+errorMsg);
		System.out.println("5-----------------"+partner);
		
		
		
		Map<String,String> messageMap = new HashMap<>();
		messageMap.put("FTPServer_Inbound", "EDI File successfully received from "+partner);
		messageMap.put("SFTPServer_Inbound", "EDI File successfully received from "+partner);
		messageMap.put("AS2_Inbound", "EDI File successfully received from "+partner);
		messageMap.put("X12_Inbound", "EDI successfully parsed");
		messageMap.put("Wrapper_Inbound", "Metadata extracted successfully from EDI");
		messageMap.put("Rest_Inbound", "Sending EDI to Queue for processing");
		messageMap.put("DataBase_Inbound", "EDI successfully persisted");
		
		messageMap.put("FTPServer_Inbound_Err", "Error while receiving EDI File from "+partner);
		messageMap.put("SFTPServer_Inbound_Err", "Error while receiving EDI File from "+partner);
		messageMap.put("AS2_Inbound_Err", "Error while receiving EDI File from "+partner);
		messageMap.put("X12_Inbound_Err", "Error while parsing EDI");
		messageMap.put("Wrapper_Inbound_Err", "Error while extracting metadata from EDI");
		messageMap.put("Rest_Inbound_Err", "Error while sending EDI to Queue");
		messageMap.put("DataBase_Inbound_Err", "Error while persisting EDI");
		
		
		messageMap.put("DataBase_Outbound", "Document generated successfully from Scheduled Jobs");
		messageMap.put("Webhook_Outbound", "Successfully received document from backend");
		messageMap.put("XmlMap_Outbound", "Document converted successfully to EDI Format");
		messageMap.put("X12_Outbound", "EDI successfully parsed");
		messageMap.put("FTP_Outbound", "EDI sent successfully to the "+partner+" through "+connector.substring(0,connector.indexOf("_")));
		messageMap.put("SFTP_Outbound", "EDI sent successfully to the "+partner+" through "+connector.substring(0,connector.indexOf("_")));
		messageMap.put("AS2_Outbound", "EDI sent successfully to the "+partner+" through "+connector.substring(0,connector.indexOf("_")));
		
		
		messageMap.put("DataBase_Outbound_Err", "Error while generating document");
		messageMap.put("Webhook_Outbound_Err", "Error while receiving document from backend");
		messageMap.put("XmlMap_Outbound_Err", "Error while converting document to EDI Format");
		messageMap.put("X12_Outbound_Err", "Error while parsing EDI");
		messageMap.put("FTP_Outbound_Err", "Error while sending EDI");
		messageMap.put("SFTP_Outbound_Err", "Error while sending EDI");
		messageMap.put("AS2_Outbound_Err", "Error while sending EDI");
		
		
		
		 DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
		   LocalDateTime now = LocalDateTime.now(); 
		   
		   
		   
	
		
		Map<String,String> resultMap =new HashMap<>();
		resultMap.put("transactionid",transactionId );
		resultMap.put("businesstrxid",businessTrxId );
		resultMap.put("timestamp",dtf.format(now) );
		resultMap.put("logMsg", messageMap.get(connector));
		resultMap.put("errormsg",errorMsg );
		
		
		return resultMap;
	}
	 
	
	
}
