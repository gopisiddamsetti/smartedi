package com.smartedi.routebuilder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonLibrary;

import com.smartedi.commons.CustomLogs;
import com.smartedi.commons.DateUtil;
import com.smartedi.commons.MapUtil;



public class SmartEDILogsRoute extends RouteBuilder {

	@Override
	public void configure() throws Exception {
		
		from("activemq:queue:SmartEDILogs").unmarshal().json(JsonLibrary.Jackson, true)
		.to("direct:SmardEDIlogsAmq");
		
		

		
		
		
	
		

		from("direct:SmardEDIlogs").from("direct:SmardEDIlogsAmq").routeId("SmartEDILogsRoute")
				.process(new Processor() {
					public void process(Exchange exchange) throws Exception {
						System.out.println("FromEndpoint--------------"+exchange.getFromEndpoint().toString());
						String transactionId = null;
						String businessTrxId = null;
						String errorMsg = null;
						String fuseErrorMsg = null;
						Object Timestamp = null;
						String outBoundPortId = null;

	                       if(exchange.getFromEndpoint().toString().equals("Endpoint[direct://OutBound810ToWebhookRoute]")) {
								
										
//								Map<String, Object> mapHeader = exchange.getIn().getHeaders();
//										if(!mapHeader.get("CamelHttpResponseCode").toString().equals("200")){
//											
//											fuseErrorMsg=mapHeader.get("CamelHttpResponseText").toString();
//											
//										}
	                    	   outBoundPortId=(String)exchange.getProperty("PortId");
	                    	   fuseErrorMsg = (String)exchange.getProperty("fuseErrorMsg");
								transactionId = (String)exchange.getProperty("transactionId");
								businessTrxId =(String)exchange.getProperty("businessTrxId");
								
								
							}
									
	           			if(exchange.getFromEndpoint().toString().equals("Endpoint[direct://SmardEDIlogs]")){
							
							Map<String, Object> map = exchange.getIn().getBody(Map.class);

							transactionId = (String) map.get("transactionId");
							businessTrxId = (String) map.get("businessTrxId");

							String fuseStatus = (String) map.get("fuseStatus");
							if (fuseStatus.equals("Failed")) {
								fuseErrorMsg = (String) map.get("fuseErrorMng");
							}
				
						}
								 
							if (exchange.getFromEndpoint().toString().equals("Endpoint[activemq://queue:SmartEDILogs]")) {

								
								Map<String, Object> map=exchange.getIn().getBody(Map.class);
								transactionId = (String) map.get("transactionId");
								businessTrxId = (String) map.get("businessTrxId");
								errorMsg =(String) map.get("errorMsg");
								

							} 
						
						
						
						
					
						 System.out.println("direct:SmardEDIlogs--------------------"+exchange.getFromEndpoint().toString());
					
						
						ProducerTemplate template = exchange.getContext().createProducerTemplate();

						Map map1 = new HashMap<>();
						map1.put("transaction-id", transactionId);
						
						List<Map<String, String>> indexId =new ArrayList<Map<String,String>>();
						List<Map<String, String>> listofRecords =new ArrayList<Map<String,String>>();
						
						
						
						System.out.println("is it--------------"+!exchange.getFromEndpoint().toString().equals("Endpoint[direct://OutBound810ToWebhookRoute]"));
						if(!exchange.getFromEndpoint().toString().equals("Endpoint[direct://OutBound810ToWebhookRoute]")) {
						System.out.println("direct:getSmartEDILogsDB InPut-----------"+map1);
						//To get Arcesb logs with Rest API call 
						listofRecords = template.requestBody("direct:getSmartEDILogs", map1,
								List.class);
						
						//To get Arcesb logs with Database call 
						/* listofRecords = template.requestBody("direct:getSmartEDILogsDB", map1,
								List.class);*/
						 
						
							for (Map<String, String> map : listofRecords) {
								
								indexId.add(MapUtil.transformLowerCase(map));
								
								
								
								
							}
						 
							int count=0;				 	 
							
	for (Map<String, String> map : indexId) {
			
		if (map.get("portid").endsWith("_Webhook_Outbound")||map.get("portid").endsWith("_FTPServer_Inbound")||map.get("portid").endsWith("_SFTPServer_Inbound")||map.get("portid").endsWith("_AS2_Inbound")) {
			count=count+1;
			System.out.println("_Webhook_Outbound || _FTPServer_Inbound ------"+map.get("connectorid"));
			    Map mapData=new HashMap<>();
			    mapData.put("transactionid", transactionId);
		mapData.put("connectorid",map.get("connectorid") );
		mapData.put("messageid",map.get("messageid"));
		mapData.put("direction", "Receive");
		mapData.put("includecontent", "True");
		
		Map m=template.requestBody("direct:postSmartEDIDataForFile", mapData,
				Map.class);

		}
									
							}
						 
						
	//condation for Re-Queue Inbound start from X12
		for (Map<String, String> map : indexId) {			 
			
				if(map.get("portid").endsWith("_X12_Inbound")&&count==0) {
					count=count+1;
					System.out.println("--------------@@@@@@@@@----------"+map.get("portid"));
					 System.out.println("int count=0;	------			 "+count);
					   Map mapData=new HashMap<>();
					    mapData.put("transactionid", transactionId);
				mapData.put("connectorid",map.get("connectorid") );
				mapData.put("messageid",map.get("messageid"));
				mapData.put("direction", "Send");
				mapData.put("includecontent", "True");
				
				Map m=template.requestBody("direct:postSmartEDIDataForFile", mapData,
						Map.class);
				}	 
							
		}				
					
							
							
							
							
							
							
							
							System.out.println("direct:getSmartEDILogsDB OutPut ArcESB Logs ------"+indexId);
						}

						
		
					
					for (Map<String, String> map : indexId) {
						System.out.println("PortId------------------"+map.get("portid"));
					}
						
						
						
						

						System.out.println("result---" + indexId);
						
						String patner="";
						if(indexId.size() != 0) {
							
							
						 patner=indexId.get(0).get("portid");
						 System.out.println("patner------------------"+patner);
						 if(patner!=null) {
								patner=patner.substring(0,patner.indexOf("_"));
								 System.out.println("patner------1------------"+patner);
							}
							
							
							//--calling log method for formated logs
							
							//--calling log method for formated logs
							
							
							indexId=	CustomLogs.getFormatLog( indexId, errorMsg, transactionId,businessTrxId);
						 
						 
						 }
						
						
						
						
						for (Map<String, String> object : indexId) {
						
							
							template.requestBody("direct:postSmartEDILogsInEs", object, Map.class);
						}
						
						
						
					/*	for (Map<String, Object> object : indexId) {
							object.put("ErrorMsg", "");
							if (object.get("Status").toString().contains("Error")) {
								object.put("ErrorMsg", errorMsg);
							}
							object.put("TransactionId", transactionId);
							object.put("BusinessTrxId", businessTrxId);
							
							template.requestBody("direct:postSmartEDILogsInEs", object, Map.class);
						}
						*/
						
					
						
						
						if(exchange.getFromEndpoint().toString().equals("Endpoint[direct://SmardEDIlogs]")){
							if (fuseErrorMsg != null) {
							System.out.println("fuseErrorMsg---------" + fuseErrorMsg);
							Map<String, String> mapOne = new HashMap<>();

							
							

							/*		mapOne.put("Filename", "");
									mapOne.put("Id", "");
									mapOne.put("PortId", "DataBase");
									mapOne.put("Status", "Send Error");
									mapOne.put("ConnectorId", "");
									mapOne.put("MessageId", "");
									mapOne.put("Timestamp", Timestamp);
									mapOne.put("ETag", "");
									mapOne.put("Direction", "Send");
									mapOne.put("FileSize", "");
									mapOne.put("FilePath", "");
									mapOne.put("ErrorMsg", fuseErrorMsg);
									mapOne.put("TransactionId", transactionId);
									mapOne.put("BusinessTrxId", businessTrxId);*/
									
									mapOne=CustomLogs.getFormatMap( transactionId, businessTrxId, "DataBase_Inbound_Err",fuseErrorMsg,patner);
							

							template.requestBody("direct:postSmartEDILogsInEs", mapOne, Map.class);
						} else {
							System.out.println("fuseErrorMsg---------" + fuseErrorMsg);
							Map<String, String> mapTwo = new HashMap<>();
					/*		mapTwo.put("Filename", "");
							mapTwo.put("Id", "");
							mapTwo.put("PortId", "DataBase");
							mapTwo.put("Status", "Sent");
							mapTwo.put("ConnectorId", "");
							mapTwo.put("MessageId", "");
							mapTwo.put("Timestamp", Timestamp);
							mapTwo.put("ETag", "");
							mapTwo.put("Direction", "Send");
							mapTwo.put("FileSize", "");
							mapTwo.put("FilePath", "");
							mapTwo.put("ErrorMsg", fuseErrorMsg);
							mapTwo.put("TransactionId", transactionId);
							mapTwo.put("BusinessTrxId", businessTrxId);*/
							
							
							mapTwo=CustomLogs.getFormatMap( transactionId, businessTrxId, "DataBase_Inbound","",patner);

							template.requestBody("direct:postSmartEDILogsInEs", mapTwo, Map.class);

						}}
						
						
						if(exchange.getFromEndpoint().toString().equals("Endpoint[direct://OutBound810ToWebhookRoute]")){
							if (fuseErrorMsg != null) {
							System.out.println("fuseErrorMsg---------" + fuseErrorMsg);
							Map<String, String> mapOne = new HashMap<>();
/*
							mapOne.put("Filename", "Invioce810xml");
							mapOne.put("Id", "");
							mapOne.put("PortId", "DataBase");
							if(outBoundPortId!=null) {
								mapOne.put("PortId", outBoundPortId);
							}
							
							mapOne.put("Status", "Send Error");
							mapOne.put("ConnectorId", "");
							mapOne.put("MessageId", "");
							mapOne.put("Timestamp", Timestamp);
							mapOne.put("ETag", "");
							mapOne.put("Direction", "Send");
							mapOne.put("FileSize", "");
							mapOne.put("FilePath", "");
							mapOne.put("ErrorMsg", fuseErrorMsg);
							mapOne.put("TransactionId", transactionId);
							mapOne.put("BusinessTrxId", businessTrxId);
System.out.println("if block calling direct:postSmartEDILogsInEs");
*/
							mapOne=CustomLogs.getFormatMap( transactionId, businessTrxId, "DataBase_Outbound_Err",fuseErrorMsg,patner);

							template.requestBody("direct:postSmartEDILogsInEs", mapOne, Map.class);
						} else {
							System.out.println("fuseErrorMsg---------" + fuseErrorMsg);
							Map<String, String> mapTwo = new HashMap<>();
							/*mapTwo.put("Filename", "Invioce810xml");
							mapTwo.put("Id", "");
							mapTwo.put("PortId", "DataBase");
							if(outBoundPortId!=null) {
								mapTwo.put("PortId", outBoundPortId);
							}
							mapTwo.put("Status", "Sent");
							mapTwo.put("ConnectorId", "");
							mapTwo.put("MessageId", "");
							mapTwo.put("Timestamp", Timestamp);
							mapTwo.put("ETag", "");
							mapTwo.put("Direction", "Send");
							mapTwo.put("FileSize", "");
							mapTwo.put("FilePath", "");
							mapTwo.put("ErrorMsg", fuseErrorMsg);
							mapTwo.put("TransactionId", transactionId);
							mapTwo.put("BusinessTrxId", businessTrxId);*/
							
							mapTwo=CustomLogs.getFormatMap( transactionId, businessTrxId, "DataBase_Outbound","",patner);

							template.requestBody("direct:postSmartEDILogsInEs", mapTwo, Map.class);

						}}

		

				

					

						exchange.getOut().setBody(indexId);

					}
				});
		
		
		//To get Arcesb logs with Rest API call 
		from("direct:getSmartEDILogs").routeId("getSmartEDILogs").process(new Processor() {
			public void process(Exchange exchange) throws Exception {

				Map map = exchange.getIn().getBody(Map.class);

				exchange.setProperty("transaction-id", map.get("transaction-id"));
			}
		}).marshal().json(JsonLibrary.Jackson, true).log("${exchangeProperty[transaction-id]}")
				.setHeader("CamelHttpMethod", constant("GET")).setHeader("Content-Type", constant("application/json"))
				.setHeader("Authorization", constant("Basic YWRtaW46M3c2UTV2OG8xSTV4OXcyUjNqN3U="))
				.log("-----------------${exchangeProperty[transaction-id]}")
				.recipientList(simple(
						"https://esb.smartedi-dev.cgdemos.com/api.rsc/transactions/?$searchableheaderfilter=((fieldname eq 'transaction-id') and (substringof('"
								+ "${exchangeProperty[transaction-id]}" + "', fieldvalue) eq True))"))
				
				.unmarshal().json(JsonLibrary.Jackson, true).process(new Processor() {
					public void process(Exchange exchange) throws Exception {
						
						exchange.getOut().setBody(exchange.getIn().getBody(Map.class).get("value"));
					}
				});

		
		
		//To get Arcesb logs with Database call 
		from("direct:getSmartEDILogsDB").routeId("getSmartEDILogsDB").log("direct:getSmartEDILogsDB--------------${body}").process(new Processor() {
			public void process(Exchange exchange) throws Exception {

				Map map = exchange.getIn().getBody(Map.class);

				exchange.setProperty("transaction-id", map.get("transaction-id"));
			}
		}).to("sqlComponent:select a.* from app_transactions a inner join app_messageheaders b on a.messageid=b.messageid where b.fieldname='transaction-id' and b.fieldvalue=:#${exchangeProperty.transaction-id}?dataSource=dataSource").log("DB--------------${body}");
		
		
		from("direct:postSmartEDILogsInEs").routeId("postSmartEDILogsInEs").process(new Processor() {
			public void process(Exchange exchange) throws Exception {
              
				Map map = exchange.getIn().getBody(Map.class);
				
			/*	map=MapUtil.transformLowerCase(map);
				map=getLogMsg(map);
				
				
				System.out.println("in=============="+map);
				
				
	
				
		
				
				map.remove("filename");
				map.remove("id");
				map.remove("portid");
				map.remove("status");
				map.remove("connectorid");
				map.remove("messageid");
				map.remove("etag");
				map.remove("direction");
				map.remove("filesize");
				map.remove("filepath");
				
				
				
				
				if(map.get("errormsg")==null) {
					
					System.out.println("errormsg----------------------"+map.get("errormsg"));
					map.put("errormsg","");
				}
				  
			
				
				System.out.println("out=============="+map);*/
				
				
				
			  
				
				
				
				
				
				exchange.getOut().setBody(map);
			}
		}).log("ES------------${body}").marshal().json(JsonLibrary.Jackson, true)
		.setHeader("CamelHttpMethod", constant("POST"))
		.setHeader("Content-Type", constant("application/json"))
		.setHeader("Authorization", constant("Basic ZWxhc3RpYzplbGFzdGlj"))
		.recipientList(simple("https://elastic.smartedi-dev.cgdemos.com/smartedilogs/edilogs"));


		
		
		
		
		from("direct:postSmartEDIDataForFile").routeId("postSmartEDIDataForFile").process(new Processor() {
			public void process(Exchange exchange) throws Exception {
              
				Map map = exchange.getIn().getBody(Map.class);
					exchange.getOut().setBody(map);
			}
		}).log("postSmartEDIDataForFileES------------${body}").marshal().json(JsonLibrary.Jackson, true)
		.setHeader("CamelHttpMethod", constant("POST"))
		.setHeader("Content-Type", constant("application/json"))
		.setHeader("Authorization", constant("Basic ZWxhc3RpYzplbGFzdGlj"))
		.recipientList(simple("https://elastic.smartedi-dev.cgdemos.com/smartedilogsfiledata/edilogsfiledata"));
		
		
		
	}
	
	
	public static Map getLogMsg(Map map5 ) {
		String logMsg=null;
		
			if(map5.get("status").toString().contains("Error")) {
			if(map5.get("status").toString().contains("Send Error")&&map5.get("direction").toString().contains("Send")) {
				logMsg="Failed to send data to "+map5.get("portid").toString().replace("_", " ")+" "+map5.get("filename");
				System.out.println("Failed to send "+map5.get("portid").toString().replace("_", " ")+" "+map5.get("filename"));
				
			}
			
	       if(map5.get("status").toString().contains("Receive Error")&&map5.get("direction").toString().contains("Recive")) {
	    	   logMsg="Recived error from "+map5.get("portid").toString().replace("_", " ")+" "+map5.get("filename");
				System.out.println("Recived error from "+map5.get("portid").toString().replace("_", " ")+" "+map5.get("filename"));
				
			}
			}else {
	       
			if(map5.get("status").toString().equals("Sent")) {
				logMsg=map5.get("filename")+" File sent successfully to "+map5.get("portid").toString().replace("_", " ");
				
				System.out.println(map5.get("filename")+" File sent successfully From "+map5.get("portid").toString().replace("_", " "));
			}
			
		if(map5.get("status").toString().equals("Received")) {
				
			logMsg=map5.get("filename")+" File Received successfully From "+map5.get("portid").toString().replace("_", " ");
			System.out.println(map5.get("filename")+" File Received successfully From "+map5.get("portid").toString().replace("_", " "));
				
			}
			}
		
			
			map5.put("logMsg", logMsg);
			
		
		return map5;
		
	
	}

}
