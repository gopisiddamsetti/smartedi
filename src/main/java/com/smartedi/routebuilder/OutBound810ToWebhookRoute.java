package com.smartedi.routebuilder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.configuration.PropertiesConfiguration;

import com.smartedi.outbound810.Transaction;
import com.smartedi.outbound810.Transaction.Data;
import com.smartedi.outbound810.Transaction.Meta;
import com.smartedi.outbound810.Transaction.Data.Invoice;
import com.smartedi.outbound810.Transaction.Data.Invoice.BaselineItemData;
import com.smartedi.outbound810.Transaction.Data.Invoice.BeginningSegmentForInvoice;
import com.smartedi.outbound810.Transaction.Data.Invoice.DateTimeReference;
import com.smartedi.outbound810.Transaction.Data.Invoice.MonetaryAmount;
import com.smartedi.outbound810.Transaction.Data.Invoice.Name;
import com.smartedi.outbound810.Transaction.Data.Invoice.ReferenceId;
import com.smartedi.outbound810.Transaction.Data.Invoice.TotalMonetaryValueSummary;
import com.smartedi.outbound810.Transaction.Data.Invoice.TransactionSetHeader;
import com.smartedi.outbound810.Transaction.Data.Invoice.BaselineItemData.BaselineItemDataIn;
import com.smartedi.outbound810.Transaction.Data.Invoice.BaselineItemData.TaxInfo;
import com.smartedi.outbound810.Transaction.Data.Invoice.Name.AddrInfo;
import com.smartedi.outbound810.Transaction.Data.Invoice.Name.GeographicLocation;
import com.smartedi.outbound810.Transaction.Data.Invoice.Name.NameIn;

public class OutBound810ToWebhookRoute extends RouteBuilder {

	@Override
	public void configure() throws Exception {
		
		


		PropertiesConfiguration properties = new PropertiesConfiguration("SmartEDI.properties");
		
	       from("scheduler://InvioceStatus?delay="+properties.getProperty("SchedulerTime")).routeId("InvioceStatusSchedulerRoute")
	       .to("sqlComponent:select * from InvoiceStatus where Status='Pending'?dataSource=dataSource").log("InvoiceStatusRoute--------------${body}")
	       .to("direct:InvoiceStatusRoute");
	       
		from("direct:InvoiceStatusRoute").routeId("InvoiceStatusRoute").process(new Processor() {
			public void process(Exchange exchange) throws Exception {
				try {
				List<Map<String, String>> result = exchange.getIn().getBody(List.class);
			if(result.size()>0) {
				
System.out.println("SchedulerTime----------------"+properties.getProperty("SchedulerTime"));
			
				properties.setProperty("SchedulerTime", "900000");

				//properties.save();
				
				System.out.println("SmartEDI.properties updated Successfully!! 900000");
				for (Map<String, String> map : result) {
					try {

						String invoiceNum=map.get("InvoiceNum");
						
						ProducerTemplate template = exchange.getContext().createProducerTemplate();

						String indexId = null;

						int InvioceUpdateCount = 0;
						int Invioce810xmlCount = 0;

						for (int a = 0; a < 5; a = a + 1) {
							try {
								System.out.println("*****************---------");
								indexId = template.requestBody("direct:InvioceUpdateStatus", map, String.class);
								InvioceUpdateCount = 1;
								if (InvioceUpdateCount == 1) {
									break;
								}
								System.out.println("indexId---------" + indexId);
							} catch (Exception e) {

								System.out.println(indexId + "--1--Exception-----" + e.getCause());
								e.printStackTrace();
							}
							TimeUnit.SECONDS.sleep(10);

						}
					String indexId1 =null;
						
						if(InvioceUpdateCount == 1) {
						try {

							indexId1 = template.requestBody("direct:OutBound810ToWebhookRoute", map, String.class);
							
							
							Invioce810xmlCount = 1;
						} catch (Exception e) {
							
						
							
							

							System.out.println(indexId1 + "--11--Exception-----" + e.getCause());
						}}

						try {
							if (Invioce810xmlCount == 1 && InvioceUpdateCount == 1) {
								properties.setProperty("SchedulerTime", "60000");

								//properties.save();
							}
							System.out.println("config.properties updated Successfully!! 60000");
						} catch (Exception e) {
							System.out.println(e.getMessage());
						}

						exchange.getOut().setBody(indexId1);

					} catch (Exception e) {
						e.printStackTrace();
					}

				}
				
				
				
				}else {
					System.out.println("list+----- size ---------------"+result.size());
				}
				}catch (Exception e) {
					System.out.println("--file prop-----------------------"+e.getMessage());
				}

			}
		});

	
		from("direct:InvioceUpdateStatus").routeId("InvioceUpdateStatusRoute").to("sqlComponent:update InvoiceStatus set Status='Processed' where InvoiceNum=:#InvoiceNum?dataSource=dataSource");
		from("direct:InvioceUpdateStatusRevert").routeId("InvioceUpdateStatusRevertRoute").to("sqlComponent:update InvoiceStatus set Status='Pending' where InvoiceNum=:#InvoiceNum?dataSource=dataSource");
		
		
		
		
	        
	     
	       
		
		
		
		
		
		
		
		
		from("direct:OutBound810ToWebhookRoute").routeId("OutBound810ToWebhookRoute")
		.to("sqlComponent:select *from InvoiceHeader where InvoiceNum=:#InvoiceNum?dataSource=dataSource")
		.process(new Processor() {
			public void process(Exchange exchange) throws Exception {
				

				List<Map<String, String>> list = exchange.getIn().getBody(List.class);


					
				Map<String,String> map=list.get(0);
				//System.out.println("InvoiceHeaderIP--------------" + map);
				
				exchange.setProperty("InvoiceNum", map.get("InvoiceNum"));
				exchange.setProperty("InvoiceHeader",map);
				
			
			}
			}).to("sqlComponent:select *from ShipToAddress where InvoiceNum=:#${exchangeProperty.InvoiceNum}?dataSource=dataSource")
        
		.process(new Processor() {
			public void process(Exchange exchange) throws Exception {


				List<Map<String, String>> list = exchange.getIn().getBody(List.class);


					
				
				//System.out.println("ShipToAddressIP--------------" + list);
				
			
				exchange.setProperty("ShipToAddress",list);
				
			
				
			
			}
			}).to("sqlComponent:select *from InvoiceLineItems where InvoiceNum=:#${exchangeProperty.InvoiceNum}?dataSource=dataSource")
        
        
		
		.process(new Processor() {
			public void process(Exchange exchange) throws Exception {
				List<Map<String, String>> list = exchange.getIn().getBody(List.class);


			
			
			//	System.out.println("InvoiceLineItemsIP--------------" + list);
				
			
				exchange.setProperty("InvoiceLineItems",list);}
			})

		.process(new Processor() {
			public void process(Exchange exchange) throws Exception {
				//-----------------------invoice DB-----------------------
				//"transactionId",  UUID.randomUUID().toString().replaceAll("-", "")
				
				
				Map<String,String> invoiceHeader=(Map<String, String>) exchange.getProperty("InvoiceHeader");
				List<Map<String, String>> shipToAddress=(List<Map<String, String>>) exchange.getProperty("ShipToAddress");
			    List<Map<String, String>> invoiceLineItems=(List<Map<String, String>>) exchange.getProperty("InvoiceLineItems");
				
			    System.out.println("InvoiceNum--------------" + exchange.getProperty("InvoiceNum"));
				System.out.println("InvoiceHeader--------------" + invoiceHeader);
				System.out.println(shipToAddress.size()+"-------ShipToAddress--------------" + shipToAddress);
				System.out.println(invoiceLineItems.size()+"-------InvoiceLineItems--------------" + invoiceLineItems);

				
				
				String receiverID=invoiceHeader.get("SupplierNum");
				String invoiceNumber=invoiceHeader.get("InvoiceNum");
				String purchaseOrderNumber=invoiceHeader.get("PONum");
				short  transactionSetIdCode=Short.parseShort("810");
				byte transactionSetControlNumber=Byte.parseByte("000001");
				
				
				Invoice invoiceDB=new Invoice();
				
				
				TransactionSetHeader transactionSetHeaderDb=new TransactionSetHeader();
				transactionSetHeaderDb.setTransactionSetIdCode(transactionSetIdCode);
				transactionSetHeaderDb.setTransactionSetControlNumber(transactionSetControlNumber);
				invoiceDB.setTransactionSetHeader(transactionSetHeaderDb);
				
				
				for (Map<String, String> map : invoiceLineItems) {
					BaselineItemData baselineItemData=new BaselineItemData();
					BaselineItemDataIn baselineItemDataIn=new BaselineItemDataIn();
					
					baselineItemDataIn.setAssignedId(map.get("InvoiceLineNum"));
					baselineItemDataIn.setProductServiceId(map.get("POLineNum"));
					baselineItemDataIn.setProductServiceIdQual( "PO");
					baselineItemDataIn.setQuantityInvoiced(map.get("Quantity"));
					baselineItemDataIn.setUnitOrBasisForMeasurementCode(map.get("UOM"));
					baselineItemDataIn.setUnitPrice(map.get("UnitPrice"));
						
					baselineItemData.setBaselineItemDataIn(baselineItemDataIn);
					
					TaxInfo taxInfo=new TaxInfo();
					taxInfo.setTaxTypeCode("SU");
					taxInfo.setMonetaryAmount(Float.parseFloat(map.get("LineAmount")));
					taxInfo.setPercent(map.get("SalesTaxPercent"));
					baselineItemData.setTaxInfo(taxInfo);
					
					invoiceDB.getBaselineItemData().add(baselineItemData);
					
				/*	map.get("InvoiceNum")
					map.get("SupplierPartNum")
					map.get("ShortDescription")
					map.get("LongDescription")
					map.get("DeliveryChargeCode")*/
					
				}
				
				

				List<JAXBElement<? extends Serializable>> dateOrInvoiceNumberOrPurchaseOrderNumberDb=new ArrayList<>();
				dateOrInvoiceNumberOrPurchaseOrderNumberDb.add(new JAXBElement(new QName("Date"), String.class, invoiceHeader.get("InvoiceDate")));
				dateOrInvoiceNumberOrPurchaseOrderNumberDb.add(new JAXBElement(new QName("InvoiceNumber"), String.class, invoiceNumber));
				dateOrInvoiceNumberOrPurchaseOrderNumberDb.add(new JAXBElement(new QName("Date"), String.class, invoiceHeader.get("TransmitDate")));
				dateOrInvoiceNumberOrPurchaseOrderNumberDb.add(new JAXBElement(new QName("PurchaseOrderNumber"), String.class, purchaseOrderNumber));
			 	BeginningSegmentForInvoice beginningSegmentForInvoiceDb=new BeginningSegmentForInvoice();
			 	
			 	
				beginningSegmentForInvoiceDb.setDateOrInvoiceNumberOrPurchaseOrderNumber(dateOrInvoiceNumberOrPurchaseOrderNumberDb);
				invoiceDB.setBeginningSegmentForInvoice(beginningSegmentForInvoiceDb);
			   	
				 
				
				ReferenceId elementDB=new ReferenceId();
				elementDB.setReferenceIdQual("ZA");
				elementDB.setReferenceId(receiverID);
				
				ReferenceId elementDB1=new ReferenceId();
				elementDB1.setReferenceIdQual("PO");
				elementDB1.setReferenceId(invoiceHeader.get("PONum"));
				
				invoiceDB.getReferenceId().add(elementDB);
				invoiceDB.getReferenceId().add(elementDB1);
			
				List<String> addrInfoListDB=new ArrayList<>();
				Name nameDB =new Name();
				AddrInfo addrInfoDB=new AddrInfo();
				for (Map<String,String> shipToAddressMap: shipToAddress) {
					NameIn nameInDB=new NameIn();
					
					nameInDB.setEntityIdCode("ST");
					nameInDB.setName(shipToAddressMap.get("Name"));
					
				
					
					nameDB.setNameIn(nameInDB);
					
							
					addrInfoListDB.add(shipToAddressMap.get("AddressLine1"));
					addrInfoDB.setAddrInfo(addrInfoListDB);
					//invoice.getName().setAddrInfo(addrInfo);
					
					
					GeographicLocation geographicLocationDB=new GeographicLocation();
					geographicLocationDB.setCityName(shipToAddressMap.get("City"));
					geographicLocationDB.setStateOrProvinceCode(shipToAddressMap.get("State"));
					geographicLocationDB.setPostalCode(Integer.parseInt(shipToAddressMap.get("Zip")));
					geographicLocationDB.setCountryCode(shipToAddressMap.get("Country"));
					//invoice.getName().setGeographicLocation(geographicLocation);
					nameDB.setGeographicLocation(geographicLocationDB);
				
					
					
				}
				
				nameDB.setAddrInfo(addrInfoDB);
				
				invoiceDB.setName(nameDB);
				
				DateTimeReference DateTimeReferenceDB=new DateTimeReference();
				short DateTimeQualDB=205;
				DateTimeReferenceDB.setDate(Integer.parseInt(invoiceHeader.get("InvoiceDate")));
				DateTimeReferenceDB.setDateTimeQual(DateTimeQualDB);
				DateTimeReference DateTimeReference1DB=new DateTimeReference();
				short DateTimeQual1DB=012;
				DateTimeReference1DB.setDate(Integer.parseInt(invoiceHeader.get("InvoiceDate")));
				DateTimeReference1DB.setDateTimeQual(DateTimeQual1DB);
				invoiceDB.getDateTimeReference().add(DateTimeReferenceDB);
				invoiceDB.getDateTimeReference().add(DateTimeReference1DB);
				
			
				
				
				
			
				List<String> amountDB=new ArrayList<>();
				amountDB.add(invoiceHeader.get("TotalInvoiceAmount"));
				
				TotalMonetaryValueSummary totalMonetaryValueSummaryDB=new TotalMonetaryValueSummary();
				totalMonetaryValueSummaryDB.setAmount(amountDB);
				invoiceDB.setTotalMonetaryValueSummary(totalMonetaryValueSummaryDB);
				
				com.smartedi.outbound810.Transaction.Data.Invoice.TaxInfo taxInfomainDB=new com.smartedi.outbound810.Transaction.Data.Invoice.TaxInfo();
				taxInfomainDB.setTaxTypeCode("SU");
				taxInfomainDB.setMonetaryAmount(Float.parseFloat(invoiceHeader.get("TotalSalesTaxAmount")));
			    invoiceDB.setTaxInfo(taxInfomainDB);
				
				
			    
				
				MonetaryAmount monetaryAmountDB=new MonetaryAmount();
				monetaryAmountDB.setAmountQualCode("D8");
				monetaryAmountDB.setMonetaryAmount(Float.parseFloat(invoiceHeader.get("DiscountPercent")));
				
				invoiceDB.setMonetaryAmount(monetaryAmountDB);
				
				//-----------------------invoice DB-----------------------
				
				
					

				
				
				exchange.setProperty("URL", invoiceHeader.get("Partner")+"/"+invoiceHeader.get("Partner")+"_X124010810"+"_Webhook_Outbound/webhook.rsb");
					
				System.out.println("URL--------URL------" + exchange.getProperty("URL"));
				

				
				
				Invoice invoice=new Invoice();
				
				
			
			
				
				TransactionSetHeader transactionSetHeader=new TransactionSetHeader();
				transactionSetHeader.setTransactionSetIdCode(transactionSetIdCode);
				transactionSetHeader.setTransactionSetControlNumber(transactionSetControlNumber);
				invoice.setTransactionSetHeader(transactionSetHeader);
				
				
				List<JAXBElement<? extends Serializable>> dateOrInvoiceNumberOrPurchaseOrderNumber=new ArrayList<>();
				dateOrInvoiceNumberOrPurchaseOrderNumber.add(new JAXBElement(new QName("Date"), String.class, "20090721"));
				dateOrInvoiceNumberOrPurchaseOrderNumber.add(new JAXBElement(new QName("InvoiceNumber"), String.class, invoiceNumber));
				dateOrInvoiceNumberOrPurchaseOrderNumber.add(new JAXBElement(new QName("Date"), String.class, "20090901"));
				dateOrInvoiceNumberOrPurchaseOrderNumber.add(new JAXBElement(new QName("PurchaseOrderNumber"), String.class, purchaseOrderNumber));
			 	BeginningSegmentForInvoice beginningSegmentForInvoice=new BeginningSegmentForInvoice();
				beginningSegmentForInvoice.setDateOrInvoiceNumberOrPurchaseOrderNumber(dateOrInvoiceNumberOrPurchaseOrderNumber);
				invoice.setBeginningSegmentForInvoice(beginningSegmentForInvoice);
			   	
				
				
				ReferenceId element=new ReferenceId();
				element.setReferenceIdQual("ZA");
				element.setReferenceId(receiverID);
				
				ReferenceId element1=new ReferenceId();
				element1.setReferenceIdQual("PO");
				element1.setReferenceId("4100ABC12300");
				
				invoice.getReferenceId().add(element);
				invoice.getReferenceId().add(element1);
			
				  
				NameIn nameIn=new NameIn();
				
				nameIn.setEntityIdCode("ST");
				nameIn.setName( "Joe");
			
				Name name =new Name();
				name.setNameIn(nameIn);
				
			
				
				
				
				AddrInfo addrInfo=new AddrInfo();
				List<String> addrInfoList=new ArrayList<>();
				addrInfoList.add("100 Westwood");
				addrInfo.setAddrInfo(addrInfoList);
				//invoice.getName().setAddrInfo(addrInfo);
				name.setAddrInfo(addrInfo);
				
				GeographicLocation geographicLocation=new GeographicLocation();
				geographicLocation.setCityName("Los Angeles");
				geographicLocation.setStateOrProvinceCode("CA");
				geographicLocation.setPostalCode(90000);
				geographicLocation.setCountryCode("US");
				//invoice.getName().setGeographicLocation(geographicLocation);
				name.setGeographicLocation(geographicLocation);
			
				
				invoice.setName(name);
				
				DateTimeReference DateTimeReference=new DateTimeReference();
				short DateTimeQual=205;
				DateTimeReference.setDate(20090721);
				DateTimeReference.setDateTimeQual(DateTimeQual);
				DateTimeReference DateTimeReference1=new DateTimeReference();
				short DateTimeQual1=012;
				DateTimeReference1.setDate(20090901);
				DateTimeReference1.setDateTimeQual(DateTimeQual1);
				invoice.getDateTimeReference().add(DateTimeReference);
				invoice.getDateTimeReference().add(DateTimeReference1);
				
			
				BaselineItemData baselineItemData=new BaselineItemData();
				BaselineItemDataIn baselineItemDataIn=new BaselineItemDataIn();
				
				baselineItemDataIn.setAssignedId("1");
				baselineItemDataIn.setQuantityInvoiced("2");
				baselineItemDataIn.setUnitOrBasisForMeasurementCode("EA");
				baselineItemDataIn.setUnitPrice("50.00");
				baselineItemDataIn.setProductServiceIdQual("PO");
				baselineItemDataIn.setProductServiceId("1");

				
				
				
					
				baselineItemData.setBaselineItemDataIn(baselineItemDataIn);
				TaxInfo taxInfo=new TaxInfo();
				taxInfo.setTaxTypeCode("SU");
				taxInfo.setMonetaryAmount(100.00f);
				taxInfo.setPercent("9.75");
				baselineItemData.setTaxInfo(taxInfo);
				
				BaselineItemData baselineItemData1=new BaselineItemData();
				BaselineItemDataIn baselineItemDataIn1=new BaselineItemDataIn();
				
				baselineItemDataIn1.setAssignedId("2");
				baselineItemDataIn1.setProductServiceIdQual("PO");
				baselineItemDataIn1.setProductServiceId("2");
				
			
				 
				baselineItemData1.setBaselineItemDataIn(baselineItemDataIn1);
				TaxInfo taxInfo1=new TaxInfo();
				taxInfo1.setTaxTypeCode("SU");
				taxInfo1.setMonetaryAmount(10.00f);
			
				baselineItemData1.setTaxInfo(taxInfo1);
				
				
				
				
				
				
				invoice.getBaselineItemData().add(baselineItemData);
				invoice.getBaselineItemData().add(baselineItemData1);
				
				List<String> amount=new ArrayList<>();
				amount.add("99.75");
				
				TotalMonetaryValueSummary totalMonetaryValueSummary=new TotalMonetaryValueSummary();
				totalMonetaryValueSummary.setAmount(amount);
				invoice.setTotalMonetaryValueSummary(totalMonetaryValueSummary);
				
				com.smartedi.outbound810.Transaction.Data.Invoice.TaxInfo taxInfomain=new com.smartedi.outbound810.Transaction.Data.Invoice.TaxInfo();
				taxInfomain.setTaxTypeCode("SU");
				taxInfomain.setMonetaryAmount(9.75f);
			    invoice.setTaxInfo(taxInfomain);
				
				
			    
				
				MonetaryAmount monetaryAmount=new MonetaryAmount();
				monetaryAmount.setAmountQualCode("D8");
				monetaryAmount.setMonetaryAmount(1.5f);
				
				invoice.setMonetaryAmount(monetaryAmount);
				  System.out.println("invoice-------810 out---" +invoice.toString());
				 // exchange.getOut().setBody(invoice);
				  
				  Transaction transaction=new Transaction();
				  System.out.println("transaction---------------"+transaction);
				  Data data=new Data();
				  data.setInvoice(invoiceDB);
				  
				  transaction.setData(data);
				  
				  Meta meta=new Meta();
				  meta.setBusinessTrxId(invoiceNumber);
				  String transactionId=UUID.randomUUID().toString().replaceAll("-", "");
				  meta.setTransactionId(transactionId);
				  transaction.setMeta(meta);
				  
				  exchange.setProperty("transactionId", transactionId);
				  exchange.setProperty("businessTrxId", invoiceNumber);
				  exchange.setProperty("PortId", "Webhook");
				  exchange.getOut().setBody(transaction);
				  ProducerTemplate template = exchange.getContext().createProducerTemplate();
				  try {
				  
			
				 Map<String,String> propMap=new HashedMap();
				 propMap.put("URL", invoiceHeader.get("Partner")+"/"+invoiceHeader.get("Partner")+"_X124010810"+"_Webhook_Outbound/webhook.rsb");
				  
				 template.getCamelContext().setProperties(propMap);
				 
				
				Map map   = template.requestBody("direct:postOutbound810toWebhook", transaction, Map.class);
				
				
				
				 System.out.println("call direct:postOutbound810toWebhook map----------"+map);
				  }catch (Exception e) {
					  System.out.println("call direct:postOutbound810toWebhook error----------"+e.getCause());
					  Map<String, String> map=new HashedMap();
					  map.put("InvoiceNum", invoiceNumber);
					template.requestBody("direct:InvioceUpdateStatusRevert", map, String.class);
					String fuseErrorMsg=e.getCause().toString();
					
					exchange.setProperty("fuseErrorMsg", fuseErrorMsg);
					
				}
			}
			
			
			
			}).to("direct:SmardEDIlogs");
		
		from("direct:postOutbound810toWebhook")
		.process(new Processor() {
			public void process(Exchange exchange) throws Exception {
				System.out.println("getProperties--------------"+exchange.getContext().getProperties());
				exchange.setProperty("URL",exchange.getContext().getProperties().get("URL"));
			}})
		.marshal("outbound810")
		
		
		.setHeader("CamelHttpMethod", constant("POST")).setHeader("Content-Type", constant("application/xml"))
		.setHeader("Authorization", constant("Basic YWRtaW46N3k0STVzMWcyWTBjN3A5SzdoOHc="))
		.recipientList(simple("https://esb.smartedi-dev.cgdemos.com/connector/"+"${exchangeProperty[URL]}"))
		.process(new Processor() {
			public void process(Exchange exchange) throws Exception {
				
				System.out.println("After API call----------------------------"+exchange.getIn().getBody(Map.class));
				
				exchange.getOut().setBody(exchange.getIn().getBody(Map.class));
			}});
		
		
		
	
       

      
          
      

      
      

    

		
	}}