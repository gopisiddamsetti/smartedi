package com.smartedi.routebuilder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;

import com.smartedi.inbound850.Transaction;
import com.smartedi.inbound850.Transaction.Data.Interchange;
import com.smartedi.inbound850.Transaction.Data.Interchange.FunctionalGroup.TransactionSet.TX00401850.PO1Loop1;

public class InBound850ToDataBaseRoute extends RouteBuilder {

	@Override
	public void configure() throws Exception {
		from("activemq:queue:InBound850ToDataBaseRoute").routeId("InBound850ToDataBaseRoute")
		.process(new Processor() {
			public void process(Exchange exchange) throws Exception {
				String errorMsg = "success";
				String transactionId = null;
				String messageId = null;
				String businessTrxId = null;

				try {

					Transaction transaction = exchange.getIn().getBody(Transaction.class);
					Interchange Interchange = transaction.getData().getInterchange();

					transactionId = transaction.getMeta().getTransactionId();
					messageId = transaction.getMeta().getMessageId();
					businessTrxId = transaction.getMeta().getBusinessTrxId();

					// System.out.println("transactionId-----------------"+transactionId);
					// System.out.println("messageId-----------------"+messageId);
					// System.out.println("businessTrxId-----------------"+businessTrxId);

					// System.out.println("Interchange-----------------"+Interchange.getFunctionalGroup().getTransactionSet().getTX00401850().getBEG().getBEG03());

					List<Map<String, String>> purchaseOrderHeader = new ArrayList<>();
					List<Map<String, String>> purchaseOrderAddress = new ArrayList<>();
					List<Map<String, String>> purchaseOrderItems = new ArrayList<>();

					Map<String, String> purchaseOrderHeaderMap = new HashMap<>();
					purchaseOrderHeaderMap.put("PurchaseOrderNumber", String.valueOf(
							Interchange.getFunctionalGroup().getTransactionSet().getTX00401850().getBEG().getBEG03()));
					purchaseOrderHeaderMap.put("OrderDate", String.valueOf(
							Interchange.getFunctionalGroup().getTransactionSet().getTX00401850().getBEG().getBEG05()));
					purchaseOrderHeader.add(purchaseOrderHeaderMap);

					Map<String, String> purchaseOrderAddressMap = new HashMap<>();

					purchaseOrderAddressMap.put("PurchaseOrderNumber", String.valueOf(
							Interchange.getFunctionalGroup().getTransactionSet().getTX00401850().getBEG().getBEG03()));
					purchaseOrderAddressMap.put("Name", Interchange.getFunctionalGroup().getTransactionSet()
							.getTX00401850().getN1Loop1().getN1().getN102());
					purchaseOrderAddressMap.put("Street", Interchange.getFunctionalGroup().getTransactionSet()
							.getTX00401850().getN1Loop1().getN3().getN301());
					purchaseOrderAddressMap.put("City", Interchange.getFunctionalGroup().getTransactionSet()
							.getTX00401850().getN1Loop1().getN4().getN401());
					purchaseOrderAddressMap.put("State", Interchange.getFunctionalGroup().getTransactionSet()
							.getTX00401850().getN1Loop1().getN4().getN402());
					purchaseOrderAddressMap.put("Zip", String.valueOf(Interchange.getFunctionalGroup()
							.getTransactionSet().getTX00401850().getN1Loop1().getN4().getN403()));
					purchaseOrderAddress.add(purchaseOrderAddressMap);

					// System.out.println("PurchaseOrder"+"------"+Interchange.getFunctionalGroup().getTransactionSet().getTX00401850().getBEG().getBEG03());
					// System.out.println("OrderDate"+"------"+Interchange.getFunctionalGroup().getTransactionSet().getTX00401850().getBEG().getBEG05());
					// System.out.println("Name"+"------"+Interchange.getFunctionalGroup().getTransactionSet().getTX00401850().getN1Loop1().getN1().getN102());
					// System.out.println("Street"+"------"+Interchange.getFunctionalGroup().getTransactionSet().getTX00401850().getN1Loop1().getN3().getN301());
					// System.out.println("City"+"------"+Interchange.getFunctionalGroup().getTransactionSet().getTX00401850().getN1Loop1().getN4().getN401());
					// System.out.println("State"+"------"+Interchange.getFunctionalGroup().getTransactionSet().getTX00401850().getN1Loop1().getN4().getN402());
					// System.out.println("Zip"+"------"+Interchange.getFunctionalGroup().getTransactionSet().getTX00401850().getN1Loop1().getN4().getN403());

					// PO1Loop1 obj =
					// Interchange.getFunctionalGroup().getTransactionSet().getTX00401850().getPO1Loop1().get(0);

					 for (PO1Loop1 obj :
				 Interchange.getFunctionalGroup().getTransactionSet().getTX00401850().getPO1Loop1())
					 {
					//PO1Loop1 obj = Interchange.getFunctionalGroup().getTransactionSet().getTX00401850().getPO1Loop1();
					// System.out.println("PartNumber"+"------"+obj.getPO1().getPO107());
					// System.out.println("ProductName"+"------"+obj.getPIDLoop1().getPID().getPID05());
					// System.out.println("Quantity"+"------"+obj.getPO1().getPO102());
					// System.out.println("USPrice"+"------"+obj.getPO1().getPO104());

					Map<String, String> mapPO1Loop1 = new HashMap<>();

					mapPO1Loop1.put("PurchaseOrderNumber", String.valueOf(
							Interchange.getFunctionalGroup().getTransactionSet().getTX00401850().getBEG().getBEG03()));
					mapPO1Loop1.put("PartNumber", obj.getPO1().getPO107());
					mapPO1Loop1.put("ProductName", obj.getPIDLoop1().getPID().getPID05());
					mapPO1Loop1.put("Quantity", String.valueOf(obj.getPO1().getPO102()));
					mapPO1Loop1.put("USPrice", String.valueOf(obj.getPO1().getPO104()));
					purchaseOrderItems.add(mapPO1Loop1);

					 }

					// exchange.getOut().setBody(listData);

					for (Map<String, String> map : purchaseOrderHeader) {
						// try {

						ProducerTemplate template = exchange.getContext().createProducerTemplate();

						String indexId = template.requestBody("direct:PurchaseOrderHeader", map, String.class);
						exchange.getOut().setBody(indexId);

						/*
						 * } catch (Exception e) {
						 * System.out.println("Exception--------0--------"+e.getLocalizedMessage());
						 * System.out.println("Exception--------1--------"+e.getMessage());
						 * System.out.println("Exception--------2--------"+e.getCause().toString());
						 * System.out.println("Exception--------3--------"+e.getClass());
						 * System.out.println("Exception--------4--------"+e.getStackTrace());
						 * System.out.println("Exception--------5--------"+e.getSuppressed());
						 * 
						 * 
						 * errorMsg=e.getMessage(); e.printStackTrace(); }
						 */

					}

					for (Map<String, String> map : purchaseOrderAddress) {
						// try {

						ProducerTemplate template = exchange.getContext().createProducerTemplate();

						String indexId = template.requestBody("direct:PurchaseOrderAddress", map, String.class);
						exchange.getOut().setBody(indexId);

						// } catch (Exception e) {
						// errorMsg=e.getMessage();
						// e.printStackTrace();
						// }

					}

					for (Map<String, String> map : purchaseOrderItems) {
						// try {

						ProducerTemplate template = exchange.getContext().createProducerTemplate();

						String indexId = template.requestBody("direct:PurchaseOrderItems", map, String.class);
						exchange.getOut().setBody(indexId);

						// } catch (Exception e) {
						// errorMsg=e.getMessage();
						// e.printStackTrace();
						// }

					}

				} catch (Exception e) {
					errorMsg = e.getCause().toString();
					System.out.println("Exception---1-------------" + errorMsg);
				}

				Map<String, Object> fuseStatus = new HashMap<>();

				if (errorMsg.equals("success")) {
					fuseStatus.put("fuseStatus", "success");
				} else {
					fuseStatus.put("fuseStatus", "Failed");
				}
				fuseStatus.put("fuseErrorMng", errorMsg);

				fuseStatus.put("transactionId", transactionId);

				fuseStatus.put("businessTrxId", businessTrxId);

				try {

					ProducerTemplate template = exchange.getContext().createProducerTemplate();

					String indexId = template.requestBody("direct:SmardEDIlogs", fuseStatus, String.class);
					exchange.getOut().setBody(indexId);

				} catch (Exception e) {
					// errorMsg=e.getMessage();

					System.out.println("Exception 2------------------------------" + e.getCause().toString());
					// e.printStackTrace();
				}

			}
		});
		
		
		from("direct:PurchaseOrderHeader").routeId("PurchaseOrderHeader").to("sqlComponent:Insert into PurchaseOrderHeader(PurchaseOrderNumber,OrderDate) values(:#PurchaseOrderNumber,:#OrderDate)?dataSource=dataSource");
		from("direct:PurchaseOrderAddress").routeId("PurchaseOrderAddress").to("sqlComponent:Insert into PurchaseOrderAddress(PurchaseOrderNumber,Name,Street,City,State,Zip) values(:#PurchaseOrderNumber,:#Name,:#Street,:#City,:#State,:#Zip)?dataSource=dataSource");
		from("direct:PurchaseOrderItems").routeId("PurchaseOrderItems").to("sqlComponent:Insert into PurchaseOrderItems(PurchaseOrderNumber,PartNumber,ProductName,Quantity,USPrice) values(:#PurchaseOrderNumber,:#PartNumber,:#ProductName,:#Quantity,:#USPrice)?dataSource=dataSource");
		
	
      
  
		
		

	}

}
