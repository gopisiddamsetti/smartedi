
package com.smartedi.outbound810;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.rssbus package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _TransactionDataInvoiceBeginningSegmentForInvoiceDate_QNAME = new QName("http://www.rssbus.com", "Date");
    private final static QName _TransactionDataInvoiceBeginningSegmentForInvoiceInvoiceNumber_QNAME = new QName("http://www.rssbus.com", "InvoiceNumber");
    private final static QName _TransactionDataInvoiceBeginningSegmentForInvoicePurchaseOrderNumber_QNAME = new QName("http://www.rssbus.com", "PurchaseOrderNumber");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.rssbus
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Transaction }
     * 
     */
    public Transaction createTransaction() {
        return new Transaction();
    }

    /**
     * Create an instance of {@link Transaction.Data }
     * 
     */
    public Transaction.Data createTransactionData() {
        return new Transaction.Data();
    }

    /**
     * Create an instance of {@link Transaction.Data.Invoice }
     * 
     */
    public Transaction.Data.Invoice createTransactionDataInvoice() {
        return new Transaction.Data.Invoice();
    }

    /**
     * Create an instance of {@link Transaction.Data.Invoice.BaselineItemData }
     * 
     */
    public Transaction.Data.Invoice.BaselineItemData createTransactionDataInvoiceBaselineItemData() {
        return new Transaction.Data.Invoice.BaselineItemData();
    }

    /**
     * Create an instance of {@link Transaction.Data.Invoice.Name }
     * 
     */
    public Transaction.Data.Invoice.Name createTransactionDataInvoiceName() {
        return new Transaction.Data.Invoice.Name();
    }

    /**
     * Create an instance of {@link Transaction.Meta }
     * 
     */
    public Transaction.Meta createTransactionMeta() {
        return new Transaction.Meta();
    }

    /**
     * Create an instance of {@link Transaction.Data.Invoice.TransactionSetHeader }
     * 
     */
    public Transaction.Data.Invoice.TransactionSetHeader createTransactionDataInvoiceTransactionSetHeader() {
        return new Transaction.Data.Invoice.TransactionSetHeader();
    }

    /**
     * Create an instance of {@link Transaction.Data.Invoice.BeginningSegmentForInvoice }
     * 
     */
    public Transaction.Data.Invoice.BeginningSegmentForInvoice createTransactionDataInvoiceBeginningSegmentForInvoice() {
        return new Transaction.Data.Invoice.BeginningSegmentForInvoice();
    }

    /**
     * Create an instance of {@link Transaction.Data.Invoice.ReferenceId }
     * 
     */
    public Transaction.Data.Invoice.ReferenceId createTransactionDataInvoiceReferenceId() {
        return new Transaction.Data.Invoice.ReferenceId();
    }

    /**
     * Create an instance of {@link Transaction.Data.Invoice.DateTimeReference }
     * 
     */
    public Transaction.Data.Invoice.DateTimeReference createTransactionDataInvoiceDateTimeReference() {
        return new Transaction.Data.Invoice.DateTimeReference();
    }

    /**
     * Create an instance of {@link Transaction.Data.Invoice.TotalMonetaryValueSummary }
     * 
     */
    public Transaction.Data.Invoice.TotalMonetaryValueSummary createTransactionDataInvoiceTotalMonetaryValueSummary() {
        return new Transaction.Data.Invoice.TotalMonetaryValueSummary();
    }

    /**
     * Create an instance of {@link Transaction.Data.Invoice.TaxInfo }
     * 
     */
    public Transaction.Data.Invoice.TaxInfo createTransactionDataInvoiceTaxInfo() {
        return new Transaction.Data.Invoice.TaxInfo();
    }

    /**
     * Create an instance of {@link Transaction.Data.Invoice.MonetaryAmount }
     * 
     */
    public Transaction.Data.Invoice.MonetaryAmount createTransactionDataInvoiceMonetaryAmount() {
        return new Transaction.Data.Invoice.MonetaryAmount();
    }

    /**
     * Create an instance of {@link Transaction.Data.Invoice.BaselineItemData.BaselineItemDataIn }
     * 
     */
    public Transaction.Data.Invoice.BaselineItemData.BaselineItemDataIn createTransactionDataInvoiceBaselineItemDataBaselineItemDataIn() {
        return new Transaction.Data.Invoice.BaselineItemData.BaselineItemDataIn();
    }

    /**
     * Create an instance of {@link Transaction.Data.Invoice.BaselineItemData.TaxInfo }
     * 
     */
    public Transaction.Data.Invoice.BaselineItemData.TaxInfo createTransactionDataInvoiceBaselineItemDataTaxInfo() {
        return new Transaction.Data.Invoice.BaselineItemData.TaxInfo();
    }

    /**
     * Create an instance of {@link Transaction.Data.Invoice.Name.NameIn }
     * 
     */
    public Transaction.Data.Invoice.Name.NameIn createTransactionDataInvoiceNameNameIn() {
        return new Transaction.Data.Invoice.Name.NameIn();
    }

    /**
     * Create an instance of {@link Transaction.Data.Invoice.Name.AddrInfo }
     * 
     */
    public Transaction.Data.Invoice.Name.AddrInfo createTransactionDataInvoiceNameAddrInfo() {
        return new Transaction.Data.Invoice.Name.AddrInfo();
    }

    /**
     * Create an instance of {@link Transaction.Data.Invoice.Name.GeographicLocation }
     * 
     */
    public Transaction.Data.Invoice.Name.GeographicLocation createTransactionDataInvoiceNameGeographicLocation() {
        return new Transaction.Data.Invoice.Name.GeographicLocation();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.rssbus.com", name = "Date", scope = Transaction.Data.Invoice.BeginningSegmentForInvoice.class)
    public JAXBElement<Long> createTransactionDataInvoiceBeginningSegmentForInvoiceDate(Long value) {
        return new JAXBElement<Long>(_TransactionDataInvoiceBeginningSegmentForInvoiceDate_QNAME, Long.class, Transaction.Data.Invoice.BeginningSegmentForInvoice.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.rssbus.com", name = "InvoiceNumber", scope = Transaction.Data.Invoice.BeginningSegmentForInvoice.class)
    public JAXBElement<String> createTransactionDataInvoiceBeginningSegmentForInvoiceInvoiceNumber(String value) {
        return new JAXBElement<String>(_TransactionDataInvoiceBeginningSegmentForInvoiceInvoiceNumber_QNAME, String.class, Transaction.Data.Invoice.BeginningSegmentForInvoice.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.rssbus.com", name = "PurchaseOrderNumber", scope = Transaction.Data.Invoice.BeginningSegmentForInvoice.class)
    public JAXBElement<String> createTransactionDataInvoiceBeginningSegmentForInvoicePurchaseOrderNumber(String value) {
        return new JAXBElement<String>(_TransactionDataInvoiceBeginningSegmentForInvoicePurchaseOrderNumber_QNAME, String.class, Transaction.Data.Invoice.BeginningSegmentForInvoice.class, value);
    }

}
