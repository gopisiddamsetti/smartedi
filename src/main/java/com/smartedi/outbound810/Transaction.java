
package com.smartedi.outbound810;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Meta">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="TransactionId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="BusinessTrxId" type="{http://www.w3.org/2001/XMLSchema}unsignedLong"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Data">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Invoice">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="TransactionSetHeader">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="TransactionSetIdCode" type="{http://www.w3.org/2001/XMLSchema}unsignedShort"/>
 *                                       &lt;element name="TransactionSetControlNumber" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="BeginningSegmentForInvoice">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;choice maxOccurs="unbounded">
 *                                         &lt;element name="Date" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/>
 *                                         &lt;element name="InvoiceNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                         &lt;element name="PurchaseOrderNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;/choice>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="ReferenceId" maxOccurs="unbounded">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="ReferenceIdQual" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="ReferenceId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="Name">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="NameIn">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="EntityIdCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                       &lt;element name="AddrInfo">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="AddrInfo" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                       &lt;element name="GeographicLocation">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="CityName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="StateOrProvinceCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="PostalCode" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/>
 *                                                 &lt;element name="CountryCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="DateTimeReference" maxOccurs="unbounded">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="DateTimeQual" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
 *                                       &lt;element name="Date" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="BaselineItemData" maxOccurs="unbounded">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="BaselineItemDataIn">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="AssignedId" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
 *                                                 &lt;element name="QuantityInvoiced" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
 *                                                 &lt;element name="UnitOrBasisForMeasurementCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="UnitPrice" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                                                 &lt;element name="ProductServiceIdQual" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="ProductServiceId" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                       &lt;element name="TaxInfo">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="TaxTypeCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="MonetaryAmount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                                                 &lt;element name="Percent" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="TotalMonetaryValueSummary">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="TaxInfo">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="TaxTypeCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="MonetaryAmount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="MonetaryAmount">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="AmountQualCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="MonetaryAmount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "meta",
    "data"
})
@XmlRootElement(name = "Transaction")
public class Transaction {

    @XmlElement(name = "Meta", required = true)
    protected Transaction.Meta meta;
    @XmlElement(name = "Data", required = true)
    protected Transaction.Data data;

    /**
     * Gets the value of the meta property.
     * 
     * @return
     *     possible object is
     *     {@link Transaction.Meta }
     *     
     */
    public Transaction.Meta getMeta() {
        return meta;
    }

    /**
     * Sets the value of the meta property.
     * 
     * @param value
     *     allowed object is
     *     {@link Transaction.Meta }
     *     
     */
    public void setMeta(Transaction.Meta value) {
        this.meta = value;
    }

    /**
     * Gets the value of the data property.
     * 
     * @return
     *     possible object is
     *     {@link Transaction.Data }
     *     
     */
    public Transaction.Data getData() {
        return data;
    }

    /**
     * Sets the value of the data property.
     * 
     * @param value
     *     allowed object is
     *     {@link Transaction.Data }
     *     
     */
    public void setData(Transaction.Data value) {
        this.data = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Invoice">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="TransactionSetHeader">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="TransactionSetIdCode" type="{http://www.w3.org/2001/XMLSchema}unsignedShort"/>
     *                             &lt;element name="TransactionSetControlNumber" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="BeginningSegmentForInvoice">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;choice maxOccurs="unbounded">
     *                               &lt;element name="Date" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/>
     *                               &lt;element name="InvoiceNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                               &lt;element name="PurchaseOrderNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;/choice>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="ReferenceId" maxOccurs="unbounded">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="ReferenceIdQual" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="ReferenceId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="Name">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="NameIn">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="EntityIdCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                             &lt;element name="AddrInfo">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="AddrInfo" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                             &lt;element name="GeographicLocation">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="CityName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="StateOrProvinceCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="PostalCode" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/>
     *                                       &lt;element name="CountryCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="DateTimeReference" maxOccurs="unbounded">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="DateTimeQual" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
     *                             &lt;element name="Date" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="BaselineItemData" maxOccurs="unbounded">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="BaselineItemDataIn">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="AssignedId" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
     *                                       &lt;element name="QuantityInvoiced" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
     *                                       &lt;element name="UnitOrBasisForMeasurementCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="UnitPrice" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                                       &lt;element name="ProductServiceIdQual" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="ProductServiceId" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                             &lt;element name="TaxInfo">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="TaxTypeCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="MonetaryAmount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                                       &lt;element name="Percent" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="TotalMonetaryValueSummary">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="TaxInfo">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="TaxTypeCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="MonetaryAmount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="MonetaryAmount">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="AmountQualCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="MonetaryAmount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "invoice"
    })
    public static class Data {

        @XmlElement(name = "Invoice", required = true)
        protected Transaction.Data.Invoice invoice;

        /**
         * Gets the value of the invoice property.
         * 
         * @return
         *     possible object is
         *     {@link Transaction.Data.Invoice }
         *     
         */
        public Transaction.Data.Invoice getInvoice() {
            return invoice;
        }

        /**
         * Sets the value of the invoice property.
         * 
         * @param value
         *     allowed object is
         *     {@link Transaction.Data.Invoice }
         *     
         */
        public void setInvoice(Transaction.Data.Invoice value) {
            this.invoice = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="TransactionSetHeader">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="TransactionSetIdCode" type="{http://www.w3.org/2001/XMLSchema}unsignedShort"/>
         *                   &lt;element name="TransactionSetControlNumber" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="BeginningSegmentForInvoice">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;choice maxOccurs="unbounded">
         *                     &lt;element name="Date" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/>
         *                     &lt;element name="InvoiceNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                     &lt;element name="PurchaseOrderNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;/choice>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="ReferenceId" maxOccurs="unbounded">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="ReferenceIdQual" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="ReferenceId" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="Name">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="NameIn">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="EntityIdCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                   &lt;element name="AddrInfo">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="AddrInfo" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                   &lt;element name="GeographicLocation">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="CityName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="StateOrProvinceCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="PostalCode" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/>
         *                             &lt;element name="CountryCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="DateTimeReference" maxOccurs="unbounded">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="DateTimeQual" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
         *                   &lt;element name="Date" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="BaselineItemData" maxOccurs="unbounded">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="BaselineItemDataIn">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="AssignedId" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
         *                             &lt;element name="QuantityInvoiced" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
         *                             &lt;element name="UnitOrBasisForMeasurementCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="UnitPrice" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *                             &lt;element name="ProductServiceIdQual" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="ProductServiceId" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                   &lt;element name="TaxInfo">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="TaxTypeCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="MonetaryAmount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *                             &lt;element name="Percent" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="TotalMonetaryValueSummary">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="TaxInfo">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="TaxTypeCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="MonetaryAmount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="MonetaryAmount">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="AmountQualCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="MonetaryAmount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "transactionSetHeader",
            "beginningSegmentForInvoice",
            "referenceId",
            "name",
            "dateTimeReference",
            "baselineItemData",
            "totalMonetaryValueSummary",
            "taxInfo",
            "monetaryAmount"
        })
        public static class Invoice {

            @XmlElement(name = "TransactionSetHeader", required = true)
            protected Transaction.Data.Invoice.TransactionSetHeader transactionSetHeader;
            @XmlElement(name = "BeginningSegmentForInvoice", required = true)
            protected Transaction.Data.Invoice.BeginningSegmentForInvoice beginningSegmentForInvoice;
            @XmlElement(name = "ReferenceId", required = true)
            protected List<Transaction.Data.Invoice.ReferenceId> referenceId;
            @XmlElement(name = "Name", required = true)
            protected Transaction.Data.Invoice.Name name;
            @XmlElement(name = "DateTimeReference", required = true)
            protected List<Transaction.Data.Invoice.DateTimeReference> dateTimeReference;
            @XmlElement(name = "BaselineItemData", required = true)
            protected List<Transaction.Data.Invoice.BaselineItemData> baselineItemData;
            @XmlElement(name = "TotalMonetaryValueSummary", required = true)
            protected Transaction.Data.Invoice.TotalMonetaryValueSummary totalMonetaryValueSummary;
            @XmlElement(name = "TaxInfo", required = true)
            protected Transaction.Data.Invoice.TaxInfo taxInfo;
            @XmlElement(name = "MonetaryAmount", required = true)
            protected Transaction.Data.Invoice.MonetaryAmount monetaryAmount;

            /**
             * Gets the value of the transactionSetHeader property.
             * 
             * @return
             *     possible object is
             *     {@link Transaction.Data.Invoice.TransactionSetHeader }
             *     
             */
            public Transaction.Data.Invoice.TransactionSetHeader getTransactionSetHeader() {
                return transactionSetHeader;
            }

            /**
             * Sets the value of the transactionSetHeader property.
             * 
             * @param value
             *     allowed object is
             *     {@link Transaction.Data.Invoice.TransactionSetHeader }
             *     
             */
            public void setTransactionSetHeader(Transaction.Data.Invoice.TransactionSetHeader value) {
                this.transactionSetHeader = value;
            }

            /**
             * Gets the value of the beginningSegmentForInvoice property.
             * 
             * @return
             *     possible object is
             *     {@link Transaction.Data.Invoice.BeginningSegmentForInvoice }
             *     
             */
            public Transaction.Data.Invoice.BeginningSegmentForInvoice getBeginningSegmentForInvoice() {
                return beginningSegmentForInvoice;
            }

            /**
             * Sets the value of the beginningSegmentForInvoice property.
             * 
             * @param value
             *     allowed object is
             *     {@link Transaction.Data.Invoice.BeginningSegmentForInvoice }
             *     
             */
            public void setBeginningSegmentForInvoice(Transaction.Data.Invoice.BeginningSegmentForInvoice value) {
                this.beginningSegmentForInvoice = value;
            }

            /**
             * Gets the value of the referenceId property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the referenceId property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getReferenceId().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link Transaction.Data.Invoice.ReferenceId }
             * 
             * 
             */
            public List<Transaction.Data.Invoice.ReferenceId> getReferenceId() {
                if (referenceId == null) {
                    referenceId = new ArrayList<Transaction.Data.Invoice.ReferenceId>();
                }
                return this.referenceId;
            }

            /**
             * Gets the value of the name property.
             * 
             * @return
             *     possible object is
             *     {@link Transaction.Data.Invoice.Name }
             *     
             */
            public Transaction.Data.Invoice.Name getName() {
                return name;
            }

            /**
             * Sets the value of the name property.
             * 
             * @param value
             *     allowed object is
             *     {@link Transaction.Data.Invoice.Name }
             *     
             */
            public void setName(Transaction.Data.Invoice.Name value) {
                this.name = value;
            }

            /**
             * Gets the value of the dateTimeReference property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the dateTimeReference property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getDateTimeReference().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link Transaction.Data.Invoice.DateTimeReference }
             * 
             * 
             */
            public List<Transaction.Data.Invoice.DateTimeReference> getDateTimeReference() {
                if (dateTimeReference == null) {
                    dateTimeReference = new ArrayList<Transaction.Data.Invoice.DateTimeReference>();
                }
                return this.dateTimeReference;
            }

            /**
             * Gets the value of the baselineItemData property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the baselineItemData property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getBaselineItemData().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link Transaction.Data.Invoice.BaselineItemData }
             * 
             * 
             */
            public List<Transaction.Data.Invoice.BaselineItemData> getBaselineItemData() {
                if (baselineItemData == null) {
                    baselineItemData = new ArrayList<Transaction.Data.Invoice.BaselineItemData>();
                }
                return this.baselineItemData;
            }

            /**
             * Gets the value of the totalMonetaryValueSummary property.
             * 
             * @return
             *     possible object is
             *     {@link Transaction.Data.Invoice.TotalMonetaryValueSummary }
             *     
             */
            public Transaction.Data.Invoice.TotalMonetaryValueSummary getTotalMonetaryValueSummary() {
                return totalMonetaryValueSummary;
            }

            /**
             * Sets the value of the totalMonetaryValueSummary property.
             * 
             * @param value
             *     allowed object is
             *     {@link Transaction.Data.Invoice.TotalMonetaryValueSummary }
             *     
             */
            public void setTotalMonetaryValueSummary(Transaction.Data.Invoice.TotalMonetaryValueSummary value) {
                this.totalMonetaryValueSummary = value;
            }

            /**
             * Gets the value of the taxInfo property.
             * 
             * @return
             *     possible object is
             *     {@link Transaction.Data.Invoice.TaxInfo }
             *     
             */
            public Transaction.Data.Invoice.TaxInfo getTaxInfo() {
                return taxInfo;
            }

            /**
             * Sets the value of the taxInfo property.
             * 
             * @param value
             *     allowed object is
             *     {@link Transaction.Data.Invoice.TaxInfo }
             *     
             */
            public void setTaxInfo(Transaction.Data.Invoice.TaxInfo value) {
                this.taxInfo = value;
            }

            /**
             * Gets the value of the monetaryAmount property.
             * 
             * @return
             *     possible object is
             *     {@link Transaction.Data.Invoice.MonetaryAmount }
             *     
             */
            public Transaction.Data.Invoice.MonetaryAmount getMonetaryAmount() {
                return monetaryAmount;
            }

            /**
             * Sets the value of the monetaryAmount property.
             * 
             * @param value
             *     allowed object is
             *     {@link Transaction.Data.Invoice.MonetaryAmount }
             *     
             */
            public void setMonetaryAmount(Transaction.Data.Invoice.MonetaryAmount value) {
                this.monetaryAmount = value;
            }

            public void setBaselineItemData(List<Transaction.Data.Invoice.BaselineItemData> baselineItemData) {
                this.baselineItemData = baselineItemData;
            }

            public void setDateTimeReference(List<Transaction.Data.Invoice.DateTimeReference> dateTimeReference) {
                this.dateTimeReference = dateTimeReference;
            }

            public void setReferenceId(List<Transaction.Data.Invoice.ReferenceId> referenceId) {
                this.referenceId = referenceId;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="BaselineItemDataIn">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="AssignedId" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
             *                   &lt;element name="QuantityInvoiced" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
             *                   &lt;element name="UnitOrBasisForMeasurementCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="UnitPrice" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
             *                   &lt;element name="ProductServiceIdQual" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="ProductServiceId" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *         &lt;element name="TaxInfo">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="TaxTypeCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="MonetaryAmount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
             *                   &lt;element name="Percent" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "baselineItemDataIn",
                "taxInfo"
            })
            public static class BaselineItemData {

                @XmlElement(name = "BaselineItemDataIn", required = true)
                protected Transaction.Data.Invoice.BaselineItemData.BaselineItemDataIn baselineItemDataIn;
                @XmlElement(name = "TaxInfo", required = true)
                protected Transaction.Data.Invoice.BaselineItemData.TaxInfo taxInfo;

                /**
                 * Gets the value of the baselineItemDataIn property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Transaction.Data.Invoice.BaselineItemData.BaselineItemDataIn }
                 *     
                 */
                public Transaction.Data.Invoice.BaselineItemData.BaselineItemDataIn getBaselineItemDataIn() {
                    return baselineItemDataIn;
                }

                /**
                 * Sets the value of the baselineItemDataIn property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Transaction.Data.Invoice.BaselineItemData.BaselineItemDataIn }
                 *     
                 */
                public void setBaselineItemDataIn(Transaction.Data.Invoice.BaselineItemData.BaselineItemDataIn value) {
                    this.baselineItemDataIn = value;
                }

                /**
                 * Gets the value of the taxInfo property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Transaction.Data.Invoice.BaselineItemData.TaxInfo }
                 *     
                 */
                public Transaction.Data.Invoice.BaselineItemData.TaxInfo getTaxInfo() {
                    return taxInfo;
                }

                /**
                 * Sets the value of the taxInfo property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Transaction.Data.Invoice.BaselineItemData.TaxInfo }
                 *     
                 */
                public void setTaxInfo(Transaction.Data.Invoice.BaselineItemData.TaxInfo value) {
                    this.taxInfo = value;
                }


                /**
                 * <p>Java class for anonymous complex type.
                 * 
                 * <p>The following schema fragment specifies the expected content contained within this class.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="AssignedId" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
                 *         &lt;element name="QuantityInvoiced" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
                 *         &lt;element name="UnitOrBasisForMeasurementCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="UnitPrice" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
                 *         &lt;element name="ProductServiceIdQual" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="ProductServiceId" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "assignedId",
                    "quantityInvoiced",
                    "unitOrBasisForMeasurementCode",
                    "unitPrice",
                    "productServiceIdQual",
                    "productServiceId"
                })
                public static class BaselineItemDataIn {

                    @XmlElement(name = "AssignedId")
                    @XmlSchemaType(name = "unsignedByte")
                    protected String assignedId;
                    @XmlElement(name = "QuantityInvoiced")
                    @XmlSchemaType(name = "unsignedByte")
                    protected String quantityInvoiced;
                    @XmlElement(name = "UnitOrBasisForMeasurementCode", required = true)
                    protected String unitOrBasisForMeasurementCode;
                    @XmlElement(name = "UnitPrice", required = true)
                    protected String unitPrice;
                    @XmlElement(name = "ProductServiceIdQual", required = true)
                    protected String productServiceIdQual;
                    @XmlElement(name = "ProductServiceId")
                    @XmlSchemaType(name = "unsignedByte")
                    protected String productServiceId;

                    /**
                     * Gets the value of the assignedId property.
                     * 
                     */
                    public String getAssignedId() {
                        return assignedId;
                    }

                    /**
                     * Sets the value of the assignedId property.
                     * 
                     */
                    public void setAssignedId(String value) {
                        this.assignedId = value;
                    }

                    /**
                     * Gets the value of the quantityInvoiced property.
                     * 
                     */
                    public String getQuantityInvoiced() {
                        return quantityInvoiced;
                    }

                    /**
                     * Sets the value of the quantityInvoiced property.
                     * 
                     */
                    public void setQuantityInvoiced(String value) {
                        this.quantityInvoiced = value;
                    }

                    /**
                     * Gets the value of the unitOrBasisForMeasurementCode property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getUnitOrBasisForMeasurementCode() {
                        return unitOrBasisForMeasurementCode;
                    }

                    /**
                     * Sets the value of the unitOrBasisForMeasurementCode property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setUnitOrBasisForMeasurementCode(String value) {
                        this.unitOrBasisForMeasurementCode = value;
                    }

                    /**
                     * Gets the value of the unitPrice property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigDecimal }
                     *     
                     */
                    public String getUnitPrice() {
                        return unitPrice;
                    }

                    /**
                     * Sets the value of the unitPrice property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigDecimal }
                     *     
                     */
                    public void setUnitPrice(String value) {
                        this.unitPrice = value;
                    }

                    /**
                     * Gets the value of the productServiceIdQual property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getProductServiceIdQual() {
                        return productServiceIdQual;
                    }

                    /**
                     * Sets the value of the productServiceIdQual property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setProductServiceIdQual(String value) {
                        this.productServiceIdQual = value;
                    }

                    /**
                     * Gets the value of the productServiceId property.
                     * 
                     */
                    public String getProductServiceId() {
                        return productServiceId;
                    }

                    /**
                     * Sets the value of the productServiceId property.
                     * 
                     */
                    public void setProductServiceId(String value) {
                        this.productServiceId = value;
                    }

                }


                /**
                 * <p>Java class for anonymous complex type.
                 * 
                 * <p>The following schema fragment specifies the expected content contained within this class.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="TaxTypeCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="MonetaryAmount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
                 *         &lt;element name="Percent" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "taxTypeCode",
                    "monetaryAmount",
                    "percent"
                })
                public static class TaxInfo {

                    @XmlElement(name = "TaxTypeCode", required = true)
                    protected String taxTypeCode;
                    @XmlElement(name = "MonetaryAmount", required = true)
                    protected Float monetaryAmount;
                    @XmlElement(name = "Percent", required = true)
                    protected String percent;

                    /**
                     * Gets the value of the taxTypeCode property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getTaxTypeCode() {
                        return taxTypeCode;
                    }

                    /**
                     * Sets the value of the taxTypeCode property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setTaxTypeCode(String value) {
                        this.taxTypeCode = value;
                    }

                    /**
                     * Gets the value of the monetaryAmount property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigDecimal }
                     *     
                     */
                    public Float getMonetaryAmount() {
                        return monetaryAmount;
                    }

                    /**
                     * Sets the value of the monetaryAmount property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigDecimal }
                     *     
                     */
                    public void setMonetaryAmount(Float value) {
                        this.monetaryAmount = value;
                    }

                    /**
                     * Gets the value of the percent property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigDecimal }
                     *     
                     */
                    public String getPercent() {
                        return percent;
                    }

                    /**
                     * Sets the value of the percent property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigDecimal }
                     *     
                     */
                    public void setPercent(String value) {
                        this.percent = value;
                    }

                }

            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;choice maxOccurs="unbounded">
             *           &lt;element name="Date" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/>
             *           &lt;element name="InvoiceNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *           &lt;element name="PurchaseOrderNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;/choice>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "dateOrInvoiceNumberOrPurchaseOrderNumber"
            })
            public static class BeginningSegmentForInvoice {

                @XmlElementRefs({
                    @XmlElementRef(name = "InvoiceNumber", namespace = "http://www.rssbus.com", type = JAXBElement.class, required = false),
                    @XmlElementRef(name = "PurchaseOrderNumber", namespace = "http://www.rssbus.com", type = JAXBElement.class, required = false),
                    @XmlElementRef(name = "Date", namespace = "http://www.rssbus.com", type = JAXBElement.class, required = false)
                })
                protected List<JAXBElement<? extends Serializable>> dateOrInvoiceNumberOrPurchaseOrderNumber;

                /**
                 * Gets the value of the dateOrInvoiceNumberOrPurchaseOrderNumber property.
                 * 
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the dateOrInvoiceNumberOrPurchaseOrderNumber property.
                 * 
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getDateOrInvoiceNumberOrPurchaseOrderNumber().add(newItem);
                 * </pre>
                 * 
                 * 
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link JAXBElement }{@code <}{@link String }{@code >}
                 * {@link JAXBElement }{@code <}{@link String }{@code >}
                 * {@link JAXBElement }{@code <}{@link Long }{@code >}
                 * 
                 * 
                 */
                public List<JAXBElement<? extends Serializable>> getDateOrInvoiceNumberOrPurchaseOrderNumber() {
                    if (dateOrInvoiceNumberOrPurchaseOrderNumber == null) {
                        dateOrInvoiceNumberOrPurchaseOrderNumber = new ArrayList<JAXBElement<? extends Serializable>>();
                    }
                    return this.dateOrInvoiceNumberOrPurchaseOrderNumber;
                }

                public void setDateOrInvoiceNumberOrPurchaseOrderNumber(List<JAXBElement<? extends Serializable>> dateOrInvoiceNumberOrPurchaseOrderNumber) {
                    this.dateOrInvoiceNumberOrPurchaseOrderNumber = dateOrInvoiceNumberOrPurchaseOrderNumber;
                }

            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="DateTimeQual" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
             *         &lt;element name="Date" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "dateTimeQual",
                "date"
            })
            public static class DateTimeReference {

                @XmlElement(name = "DateTimeQual")
                @XmlSchemaType(name = "unsignedByte")
                protected short dateTimeQual;
                @XmlElement(name = "Date")
                @XmlSchemaType(name = "unsignedInt")
                protected long date;

                /**
                 * Gets the value of the dateTimeQual property.
                 * 
                 */
                public short getDateTimeQual() {
                    return dateTimeQual;
                }

                /**
                 * Sets the value of the dateTimeQual property.
                 * 
                 */
                public void setDateTimeQual(short value) {
                    this.dateTimeQual = value;
                }

                /**
                 * Gets the value of the date property.
                 * 
                 */
                public long getDate() {
                    return date;
                }

                /**
                 * Sets the value of the date property.
                 * 
                 */
                public void setDate(long value) {
                    this.date = value;
                }

            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="AmountQualCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="MonetaryAmount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "amountQualCode",
                "monetaryAmount"
            })
            public static class MonetaryAmount {

                @XmlElement(name = "AmountQualCode", required = true)
                protected String amountQualCode;
                @XmlElement(name = "MonetaryAmount", required = true)
                protected Float monetaryAmount;

                /**
                 * Gets the value of the amountQualCode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAmountQualCode() {
                    return amountQualCode;
                }

                /**
                 * Sets the value of the amountQualCode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAmountQualCode(String value) {
                    this.amountQualCode = value;
                }

                /**
                 * Gets the value of the monetaryAmount property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public Float getMonetaryAmount() {
                    return monetaryAmount;
                }

                /**
                 * Sets the value of the monetaryAmount property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setMonetaryAmount(Float value) {
                    this.monetaryAmount = value;
                }

            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="NameIn">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="EntityIdCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *         &lt;element name="AddrInfo">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="AddrInfo" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *         &lt;element name="GeographicLocation">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="CityName" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="StateOrProvinceCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="PostalCode" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/>
             *                   &lt;element name="CountryCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "nameIn",
                "addrInfo",
                "geographicLocation"
            })
            public static class Name {

                @XmlElement(name = "NameIn", required = true)
                protected Transaction.Data.Invoice.Name.NameIn nameIn;
                @XmlElement(name = "AddrInfo", required = true)
                protected Transaction.Data.Invoice.Name.AddrInfo addrInfo;
                @XmlElement(name = "GeographicLocation", required = true)
                protected Transaction.Data.Invoice.Name.GeographicLocation geographicLocation;

                /**
                 * Gets the value of the nameIn property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Transaction.Data.Invoice.Name.NameIn }
                 *     
                 */
                public Transaction.Data.Invoice.Name.NameIn getNameIn() {
                    return nameIn;
                }

                /**
                 * Sets the value of the nameIn property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Transaction.Data.Invoice.Name.NameIn }
                 *     
                 */
                public void setNameIn(Transaction.Data.Invoice.Name.NameIn value) {
                    this.nameIn = value;
                }

                /**
                 * Gets the value of the addrInfo property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Transaction.Data.Invoice.Name.AddrInfo }
                 *     
                 */
                public Transaction.Data.Invoice.Name.AddrInfo getAddrInfo() {
                    return addrInfo;
                }

                /**
                 * Sets the value of the addrInfo property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Transaction.Data.Invoice.Name.AddrInfo }
                 *     
                 */
                public void setAddrInfo(Transaction.Data.Invoice.Name.AddrInfo value) {
                    this.addrInfo = value;
                }

                /**
                 * Gets the value of the geographicLocation property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Transaction.Data.Invoice.Name.GeographicLocation }
                 *     
                 */
                public Transaction.Data.Invoice.Name.GeographicLocation getGeographicLocation() {
                    return geographicLocation;
                }

                /**
                 * Sets the value of the geographicLocation property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Transaction.Data.Invoice.Name.GeographicLocation }
                 *     
                 */
                public void setGeographicLocation(Transaction.Data.Invoice.Name.GeographicLocation value) {
                    this.geographicLocation = value;
                }


                /**
                 * <p>Java class for anonymous complex type.
                 * 
                 * <p>The following schema fragment specifies the expected content contained within this class.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="AddrInfo" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "addrInfo"
                })
                public static class AddrInfo {

                    @XmlElement(name = "AddrInfo", required = true)
                    protected List<String> addrInfo;

                    /**
                     * Gets the value of the addrInfo property.
                     * 
                     * <p>
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a <CODE>set</CODE> method for the addrInfo property.
                     * 
                     * <p>
                     * For example, to add a new item, do as follows:
                     * <pre>
                     *    getAddrInfo().add(newItem);
                     * </pre>
                     * 
                     * 
                     * <p>
                     * Objects of the following type(s) are allowed in the list
                     * {@link String }
                     * 
                     * 
                     */
                    public List<String> getAddrInfo() {
                        if (addrInfo == null) {
                            addrInfo = new ArrayList<String>();
                        }
                        return this.addrInfo;
                    }

                    public void setAddrInfo(List<String> addrInfo) {
                        this.addrInfo = addrInfo;
                    }

                }


                /**
                 * <p>Java class for anonymous complex type.
                 * 
                 * <p>The following schema fragment specifies the expected content contained within this class.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="CityName" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="StateOrProvinceCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="PostalCode" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/>
                 *         &lt;element name="CountryCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "cityName",
                    "stateOrProvinceCode",
                    "postalCode",
                    "countryCode"
                })
                public static class GeographicLocation {

                    @XmlElement(name = "CityName", required = true)
                    protected String cityName;
                    @XmlElement(name = "StateOrProvinceCode", required = true)
                    protected String stateOrProvinceCode;
                    @XmlElement(name = "PostalCode")
                    @XmlSchemaType(name = "unsignedInt")
                    protected long postalCode;
                    @XmlElement(name = "CountryCode", required = true)
                    protected String countryCode;

                    /**
                     * Gets the value of the cityName property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getCityName() {
                        return cityName;
                    }

                    /**
                     * Sets the value of the cityName property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setCityName(String value) {
                        this.cityName = value;
                    }

                    /**
                     * Gets the value of the stateOrProvinceCode property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getStateOrProvinceCode() {
                        return stateOrProvinceCode;
                    }

                    /**
                     * Sets the value of the stateOrProvinceCode property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setStateOrProvinceCode(String value) {
                        this.stateOrProvinceCode = value;
                    }

                    /**
                     * Gets the value of the postalCode property.
                     * 
                     */
                    public long getPostalCode() {
                        return postalCode;
                    }

                    /**
                     * Sets the value of the postalCode property.
                     * 
                     */
                    public void setPostalCode(long value) {
                        this.postalCode = value;
                    }

                    /**
                     * Gets the value of the countryCode property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getCountryCode() {
                        return countryCode;
                    }

                    /**
                     * Sets the value of the countryCode property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setCountryCode(String value) {
                        this.countryCode = value;
                    }

                }


                /**
                 * <p>Java class for anonymous complex type.
                 * 
                 * <p>The following schema fragment specifies the expected content contained within this class.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="EntityIdCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "entityIdCode",
                    "name"
                })
                public static class NameIn {

                    @XmlElement(name = "EntityIdCode", required = true)
                    protected String entityIdCode;
                    @XmlElement(name = "Name", required = true)
                    protected String name;

                    /**
                     * Gets the value of the entityIdCode property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getEntityIdCode() {
                        return entityIdCode;
                    }

                    /**
                     * Sets the value of the entityIdCode property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setEntityIdCode(String value) {
                        this.entityIdCode = value;
                    }

                    /**
                     * Gets the value of the name property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getName() {
                        return name;
                    }

                    /**
                     * Sets the value of the name property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setName(String value) {
                        this.name = value;
                    }

                }

            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="ReferenceIdQual" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="ReferenceId" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "referenceIdQual",
                "referenceId"
            })
            public static class ReferenceId {

                @XmlElement(name = "ReferenceIdQual", required = true)
                protected String referenceIdQual;
                @XmlElement(name = "ReferenceId", required = true)
                protected String referenceId;

                /**
                 * Gets the value of the referenceIdQual property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getReferenceIdQual() {
                    return referenceIdQual;
                }

                /**
                 * Sets the value of the referenceIdQual property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setReferenceIdQual(String value) {
                    this.referenceIdQual = value;
                }

                /**
                 * Gets the value of the referenceId property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getReferenceId() {
                    return referenceId;
                }

                /**
                 * Sets the value of the referenceId property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setReferenceId(String value) {
                    this.referenceId = value;
                }

            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="TaxTypeCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="MonetaryAmount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "taxTypeCode",
                "monetaryAmount"
            })
            public static class TaxInfo {

                @XmlElement(name = "TaxTypeCode", required = true)
                protected String taxTypeCode;
                @XmlElement(name = "MonetaryAmount", required = true)
                protected float monetaryAmount;

                /**
                 * Gets the value of the taxTypeCode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getTaxTypeCode() {
                    return taxTypeCode;
                }

                /**
                 * Sets the value of the taxTypeCode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setTaxTypeCode(String value) {
                    this.taxTypeCode = value;
                }

                /**
                 * Gets the value of the monetaryAmount property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public float getMonetaryAmount() {
                    return monetaryAmount;
                }

                /**
                 * Sets the value of the monetaryAmount property.
                 * 
                 * @param f
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setMonetaryAmount(float f) {
                    this.monetaryAmount = f;
                }

            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "amount"
            })
            public static class TotalMonetaryValueSummary {

                @XmlElement(name = "Amount", required = true)
                protected List<String> amount;

                /**
                 * Gets the value of the amount property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public List<String> getAmount() {
                    return amount;
                }

                /**
                 * Sets the value of the amount property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setAmount(List<String> value) {
                    this.amount = value;
                }

            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="TransactionSetIdCode" type="{http://www.w3.org/2001/XMLSchema}unsignedShort"/>
             *         &lt;element name="TransactionSetControlNumber" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "transactionSetIdCode",
                "transactionSetControlNumber"
            })
            public static class TransactionSetHeader {

                @XmlElement(name = "TransactionSetIdCode")
                @XmlSchemaType(name = "unsignedShort")
                protected int transactionSetIdCode;
                @XmlElement(name = "TransactionSetControlNumber")
                @XmlSchemaType(name = "unsignedByte")
                protected short transactionSetControlNumber;

                /**
                 * Gets the value of the transactionSetIdCode property.
                 * 
                 */
                public int getTransactionSetIdCode() {
                    return transactionSetIdCode;
                }

                /**
                 * Sets the value of the transactionSetIdCode property.
                 * 
                 */
                public void setTransactionSetIdCode(int value) {
                    this.transactionSetIdCode = value;
                }

                /**
                 * Gets the value of the transactionSetControlNumber property.
                 * 
                 */
                public short getTransactionSetControlNumber() {
                    return transactionSetControlNumber;
                }

                /**
                 * Sets the value of the transactionSetControlNumber property.
                 * 
                 */
                public void setTransactionSetControlNumber(short value) {
                    this.transactionSetControlNumber = value;
                }

            }

        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="TransactionId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="BusinessTrxId" type="{http://www.w3.org/2001/XMLSchema}unsignedLong"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "transactionId",
        "businessTrxId"
    })
    public static class Meta {

        @XmlElement(name = "TransactionId", required = true)
        protected String transactionId;
        @XmlElement(name = "BusinessTrxId", required = true)
        @XmlSchemaType(name = "unsignedLong")
        protected String businessTrxId;

        /**
         * Gets the value of the transactionId property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTransactionId() {
            return transactionId;
        }

        /**
         * Sets the value of the transactionId property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTransactionId(String value) {
            this.transactionId = value;
        }

        /**
         * Gets the value of the businessTrxId property.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public String getBusinessTrxId() {
            return businessTrxId;
        }

        /**
         * Sets the value of the businessTrxId property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setBusinessTrxId(String value) {
            this.businessTrxId = value;
        }

    }

}
