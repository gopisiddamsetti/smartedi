-- ---------------------------------------------------850---------------------------------------
use arcesbdata;
drop table PurchaseOrderAddress;
drop table PurchaseOrderItems;
drop table PurchaseOrderHeader;

CREATE TABLE `PurchaseOrderHeader` (
  `PurchaseOrderNumber` varchar(100) ,
  `OrderDate` varchar(100) DEFAULT NULL,PRIMARY KEY (PurchaseOrderNumber)
  );


CREATE TABLE `PurchaseOrderAddress` (
  `PurchaseOrderNumber` varchar(100) ,
  `Name` varchar(100) DEFAULT NULL,  
  `Street` varchar(100) DEFAULT NULL,
  `City` varchar(100) DEFAULT NULL,
  `State` varchar(100) DEFAULT NULL,
  `Zip` varchar(100) DEFAULT NULL,
   FOREIGN KEY (PurchaseOrderNumber) REFERENCES PurchaseOrderHeader(PurchaseOrderNumber)
  );

CREATE TABLE `PurchaseOrderItems` (
  `PurchaseOrderNumber` varchar(100) ,
  `PartNumber` varchar(100) DEFAULT NULL,  
  `ProductName` varchar(100) DEFAULT NULL,  
  `Quantity` varchar(100) DEFAULT NULL,
  `USPrice` varchar(100) DEFAULT NULL,
   FOREIGN KEY (PurchaseOrderNumber) REFERENCES PurchaseOrderHeader(PurchaseOrderNumber)
  );
  
Insert into PurchaseOrderHeader(PurchaseOrderNumber,OrderDate) values('08292233294','11/27/2010');
  
Insert into PurchaseOrderAddress(PurchaseOrderNumber,Name,Street,City,State,Zip) values('08292233294','Joe','31875 SOLON RD','SOLON','OH','44139');
  
Insert into PurchaseOrderItems(PurchaseOrderNumber,PartNumber,ProductName,Quantity,USPrice) 
values('08292233294','065322-117','SMALL WIDGET','120','9.25');
Insert into PurchaseOrderItems(PurchaseOrderNumber,PartNumber,ProductName,Quantity,USPrice) 
values('08292233294','066850-116','MEDIUM WIDGET','220','13.79');
Insert into PurchaseOrderItems(PurchaseOrderNumber,PartNumber,ProductName,Quantity,USPrice) 
values('08292233294','060733-110"','LARGE WIDGET','126','10.99');

select * from PurchaseOrderHeader;
select * from PurchaseOrderAddress;
select * from PurchaseOrderItems;

-- ---------------------------------------------------850---------------------------------------
-- ---------------------------------------------------810---------------------------------------
use arcesbdata;
drop table ShipToAddress;
drop table InvoiceLineItems;
drop table InvoiceHeader;
drop table InvoiceStatus;

CREATE TABLE `InvoiceStatus` (
  `InvoiceNum` varchar(25) ,
  `Status` varchar(9) DEFAULT NULL,
  PRIMARY KEY (InvoiceNum)
  );


CREATE TABLE `InvoiceHeader` (
  `InvoiceNum` varchar(100) ,
  `DocumentSource` varchar(100) DEFAULT NULL,
  `DocumentType` varchar(100) DEFAULT NULL,
  `DocumentVersion` varchar(100) DEFAULT NULL,
  `DocumentProcessCode` varchar(100) DEFAULT NULL,
  `DocumentRef` varchar(100) DEFAULT NULL,
  `TransmitLoginId` varchar(100) DEFAULT NULL,
  `TransmitDate` varchar(100) DEFAULT NULL,
  `Organization` varchar(100) DEFAULT NULL,
  `SupplierNum` varchar(100) DEFAULT NULL,
  `PONum` varchar(100) DEFAULT NULL,
  `InvoiceDate` varchar(100) DEFAULT NULL,
  `TotalInvoiceAmount` varchar(100) DEFAULT NULL,
  `TotalSalesTaxAmount` varchar(100) DEFAULT NULL,
  `DiscountAmount` varchar(100) DEFAULT NULL,
  `DiscountPercent` varchar(100) DEFAULT NULL,
  `DiscountDueDate` varchar(100) DEFAULT NULL,
  `Partner` varchar(100) DEFAULT NULL,
  PRIMARY KEY (InvoiceNum)
  );

CREATE TABLE `ShipToAddress` (
  `InvoiceNum` varchar(100) ,
  `Name` varchar(100) DEFAULT NULL,
  `AddressLine1` varchar(100) DEFAULT NULL,
  `AddressLine2` varchar(100) DEFAULT NULL,
  `City` varchar(100) DEFAULT NULL,
  `State` varchar(100) DEFAULT NULL,
  `Zip` varchar(100) DEFAULT NULL,
  `Country` varchar(100) DEFAULT NULL,
  FOREIGN KEY (InvoiceNum) REFERENCES InvoiceHeader(InvoiceNum)
  );
 
CREATE TABLE `InvoiceLineItems` (
  `InvoiceNum` varchar(100) ,
  `InvoiceLineNum` varchar(100) DEFAULT NULL,
  `POLineNum` varchar(100) DEFAULT NULL,
  `Quantity` varchar(100) DEFAULT NULL,
  `UOM` varchar(100) DEFAULT NULL,
  `UnitPrice` varchar(100) DEFAULT NULL,
  `LineAmount` varchar(100) DEFAULT NULL,
  `SalesTaxPercent` varchar(100) DEFAULT NULL,
  `SupplierPartNum` varchar(100) DEFAULT NULL,
  `ShortDescription` varchar(100) DEFAULT NULL,
  `LongDescription` varchar(100) DEFAULT NULL,
  `DeliveryChargeCode` varchar(100) DEFAULT NULL,
   FOREIGN KEY (InvoiceNum) REFERENCES InvoiceHeader(InvoiceNum)
  );
  
  Insert into InvoiceHeader(InvoiceNum,DocumentSource,DocumentType,DocumentVersion,DocumentProcessCode,DocumentRef,TransmitLoginId,TransmitDate,Organization,SupplierNum,PONum,InvoiceDate,TotalInvoiceAmount,TotalSalesTaxAmount,DiscountAmount,DiscountPercent,DiscountDueDate,Partner) 
values('INV-1234567890','Supplier','Invoice','1.0.0','','Ref-123varchar1006789','ABCDE123varchar100','20090721','CAN','AMZ','4100ABC12300','20090721','99.75','9.75','','1.5','9/1/2009','Amazon');

  Insert into ShipToAddress(InvoiceNum,Name,AddressLine1,AddressLine2,City,State,Zip,Country) 
values('INV-1234567890','Joe','100 Westwood','','Los Angeles','CA','90000','USA');
Insert into ShipToAddress(InvoiceNum,Name,AddressLine1,AddressLine2,City,State,Zip,Country) 
values('INV-1234567890','Mark','100 Westwood','','Los Angeles','CA','90000','USA');

  
  
Insert into InvoiceLineItems(InvoiceNum,InvoiceLineNum,POLineNum,Quantity,UOM,UnitPrice,LineAmount,SalesTaxPercent,SupplierPartNum,ShortDescription,LongDescription,DeliveryChargeCode) 
values('INV-1234567890','1','1','2','EA','50.00','100.00','9.75','','Marley &amp; Me','Marley &amp; Me','');
Insert into InvoiceLineItems(InvoiceNum,InvoiceLineNum,POLineNum,Quantity,UOM,UnitPrice,LineAmount,SalesTaxPercent,SupplierPartNum,ShortDescription,LongDescription,DeliveryChargeCode) 
values('INV-1234567890','2','2','5','EA','50.00','100.00','9.75','','Marley &amp; Me','Marley &amp; Me','');
Insert into InvoiceLineItems(InvoiceNum,InvoiceLineNum,POLineNum,Quantity,UOM,UnitPrice,LineAmount,SalesTaxPercent,SupplierPartNum,ShortDescription,LongDescription,DeliveryChargeCode) 
values('INV-1234567890','3','3','6','EA','50.00','100.00','9.75','','Marley &amp; Me','Marley &amp; Me','');


-- InvoiceStatus ---
Insert into InvoiceStatus(InvoiceNum,Status) 
values('INV-1234567890','Pending');
update InvoiceStatus set Status='Processed' where InvoiceNum='INV-1234567890';
-- InvoiceStatus ---

select * from InvoiceStatus where Status='Pending';
select * from InvoiceHeader;
select * from ShipToAddress;
select * from InvoiceLineItems;


-- ---------------------------------------------------810---------------------------------------